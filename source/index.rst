.. include:: ___header.rst

Software Engineering by ALT-F1
==============================

Building software requires multiple competencies : understand the business, the regulations, the IT system, the operations, the testing process, the `technical debt`_ ...

By reading this book, you should find sufficient information to manage the manufacturing of software in a systematic method.

Contact us : http://www.alt-f1.be

* OUR COMMITMENT : We strive for superior performance, unmatched work ethic, simple and pragmatic approach, jargon-free language and insightful ideas

* ALT-F1 supports your industry with software, data, analytics & lean optimisations

* ALT-F1 designs, implements, deploys and supports secure, large-scale software solutions for diverse industries: 

    * Manufacturing
    * MRO Maintenance Repair and Overhaul
    * Warehouse
    * Broadcasting
    * Bank
    * Insurance 
    * Defense
    * Automotive 
    * Law Enforcement 
    * Justice and Serious International Crime

.. toctree::
   :maxdepth: 3
   :numbered:
   :caption: Contents
   
   00-00.Introduction.rst
   01-01.Inbound_Logistics.rst
   01-02.Operations.rst
   01-03.Outbound_Logistics.rst
   01-04.Marketing_and_Sales.rst
   01-05.Services.rst
   02-01.Procurement.rst
   02-02.Technology_development.rst
   02-03.HR_management.rst 
   02-04.Firm_infrastructure.rst
   99-99.Misc.rst
   glossary
   
   secure_and_private_ai/README.md
   secure_and_private_ai/jupyter-notebook/chap1/Part_1-Tensors_in_PyTorch-Exercises.ipynb
   secure_and_private_ai/jupyter-notebook/chap1/Part_2-Neural_Networks_in_PyTorch_Exercises.ipynb
   secure_and_private_ai/jupyter-notebook/chap1/Part_3-Training_Neural_Networks_Solution.ipynb
   secure_and_private_ai/jupyter-notebook/chap1/Part_4-Fashion-MNIST_Solution.ipynb 


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`