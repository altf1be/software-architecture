.. "=  - # ` : ' " ~ ^ _ *  < >"

The digitization
=============================

The :term:`Digitization` is *"Digitization, less commonly digitalization, is the process of converting information into a digital (i.e. computer-readable) format, in which the information is organized into bits."*

In short, developing software mimicking manual processes while adding new capabilities such enabling the communication between IT systems from different companies that couldn't have been integrated using another way.

.. include:: _ALT-F1-02-02-TechnoDev-Digitization-Insurance.rst
