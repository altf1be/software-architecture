.. the header contains shared information by documentation pages

.. include secure_and_private_ai/___header.rst

.. URL Links

.. _innoviris_soutien_financier: https://innoviris.brussels/fr/corporate-spin

.. _use-the-authorization-code-to-request-an-access-token: https://docs.microsoft.com/en-us/azure/active-directory/develop/v1-protocols-oauth-code#use-the-authorization-code-to-request-an-access-token

.. _technical debt: https://en.wikipedia.org/wiki/Technical_debt

.. _Porter value chain: https://s3.amazonaws.com/academia.edu.documents/43857184/Competitive_Advantage-_creative_and_sustaining.pdf?AWSAccessKeyId=AKIAIWOWYYGZ2Y53UL3A&Expires=1556379746&Signature=mJOvChdTYMgqjhGcjKah%2BfMam9Q%3D&response-content-disposition=inline%3B%20filename%3DCompetitive_Advantage-_creative_and_sust.pdf


.. _Insurance claims analysis: https://demo.boldbi.com/bi/en-us/dashboards/ad50e869-4fe8-4e8f-9137-54c3aec5ee54/insurance/insurance%20analysis%20dashboard

.. _MiFID 2: https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=celex%3A32014L0065

.. _Solvency 2: https://ec.europa.eu/info/law/markets-financial-instruments-mifid-ii-directive-2014-65-eu_en

.. _Intro to PyTorch - Notebook Workspace: https://classroom.udacity.com/courses/ud185/lessons/8a993162-65c4-4a80-bd35-47d9f3a6f5bc/concepts/70526adf-40d3-4446-ac32-d3f798739745

.. _Testing and Debugging ASP.NET Web API: https://docs.microsoft.com/en-us/aspnet/web-api/overview/testing-and-debugging

.. _The Joel Test - 12 Steps to Better Code: https://www.joelonsoftware.com/2000/08/09/the-joel-test-12-steps-to-better-code

.. _Continuous integration vs. continuous delivery vs. continuous deployment: https://www.atlassian.com/continuous-delivery/principles/continuous-integration-vs-delivery-vs-deployment

.. _The Simple Programmer Test: https://simpleprogrammer.com/joel-test-programmers-simple-programmer-test

.. _The Joel Test For Programmers (The Simple Programmer Test): https://simpleprogrammer.com/joel-test-programmers-simple-programmer-test

.. Substitutions of Images

.. |stratex_data_model.png| image:: _static/img/stratex_data_model-700x525.png
			:alt: StratEx Data model
			:width: 700 px
			:height: 525 px
			:scale: 100 %

.. |gitmodel.png| image:: _static/img/git-model_2x-700x927.png
			:alt: By Vincent Driessen on Tuesday, January 05, 2010
			:width: 700 px
			:height: 927 px
			:scale: 100 %

.. |baremetrics.io-dashboard.png| image:: _static/img/baremetrics.io-dashboard-700x408.png
			:alt: financial dashboard - credits: baremetrics.io
			:width: 700 px
			:height: 408 px
			:scale: 100 %

.. |tfs.ci-cd-process.png| image:: _static/img/visualstudio.microsoft.com/AnyDeveloperAnyLanguage_636x350-op.png
			:alt: CI/CD process by Team Foundation Service - credits: microsoft.com
			:width: 636 px
			:height: 350 px
			:scale: 100 %

.. |Michael_Porters_Value_Chain.png| image:: _static/img/business-to-you.com/Value-Chain-1198x619.png 
			:alt: Michael Porter's Value Chain - source: business-to-you.com
			:width: 1198 px
			:height: 619 px
			:scale: 100 %


.. |Michael_Porters_Value_Chain-634x311.png| image:: _static/img/wikipedia.com/Michael_Porters_Value_Chain-634x311.png
			:alt: Michael Porter's Value Chain. Denis Fadeev [CC BY-SA 3.0 (https://creativecommons.org/licenses/by-sa/3.0)]
			:width: 700 px
			:height: 369 px
			:scale: 100 %

.. |cicd-CIvsCIvsCD.png| image:: _static/img/atlassian.com/cicd-CIvsCIvsCD-700x393.png
			:alt: Continous Integration vs. Continous Delivery vs. Continuous Development - credits : atlassian.com
			:width: 700 px
			:height: 393 px
			:scale: 100 %
			

.. |choosing-a-good-chart-09-1-2000x1700.png| image:: _static/pdf/extremepresentation.typepad.com/choosing-a-good-chart-09-1-2000x1700.png
			:alt: Chart Suggestions - A Thought-Starter - credits : (c) A. Abela - a.v.abela@gmail.com
			:width: 1700 px
			:height: 2000 px
			:scale: 50 %

.. |choosing-a-good-chart-09-1-639x494.png| image:: _static/pdf/extremepresentation.typepad.com/choosing-a-good-chart-09-1-639x494.png 
			:alt: Chart Suggestions - A Thought-Starter - credits : (c) A. Abela - a.v.abela@gmail.com
			:width: 639 px
			:height: 494 px
			:scale: 100 %

.. |ALT-F1-Bot-Screenshot_1080x1920.png| image:: _static/img/alt-f1.be/ALT-F1-Bot-Screenshot_1080x1920.png
			:alt: Chatbot - book a train
			:width: 1080 px
			:height: 1920 px
			:scale: 25 %
			:align: middle

.. |ALT-F1-Bot-Screenshot_400x711.png| image:: _static/img/alt-f1.be/ALT-F1-Bot-Screenshot_400x711.png
			:alt: Chatbot - book a train
			:width: 400 px
			:height: 711 px
			:scale: 100 %
			:align: middle

.. |ALT-F1-Dunning_service-Swagger-API-Documentation.PNG| image:: _static/img/alt-f1.be/ALT-F1-Dunning_service-Swagger-API-Documentation.PNG
			:alt: API Documentation: example of a Dunning service
			:width: 1861 px
			:height: 1587 px
			:scale: 25 %
			:align: middle

.. |policy-claim-detailed-analysis.png| image:: _static/img/syncfusion.com/policy-claim-detailed-analysis.png 
			:alt: Middle office: Insurance Claims Analysis (credits: syncfusion.com)
			:width: 2050 px
			:height: 1066 px
			:scale: 25 %
			:align: middle