.. "=  - # ` : ' " ~ ^ _ *  < >"

.. include:: ___header.rst

Project management
==================

StratEx is a web application enabling Managers to control the delivery using customer’ processes enforced by conventions shared amongst the team members.

- StratEx App: https://www.stratexapp.com 
- StratEx App documentation : https://doc.stratexapp.com

|stratex_data_model.png|