.. "=  - # ` : ' " ~ ^ _ *  < >"

GDPR - Règlement général sur la protection des données
======================================================

*Le règlement no 2016/679, dit règlement général sur la protection des
données (RGPD, ou encore GDPR, de l’anglais General Data Protection
Regulation), est un règlement de l’Union européenne qui constitue le
texte de référence en matière de protection des données à caractère
personnel1. Il renforce et unifie la protection des données pour les
individus au sein de l’Union européenne.*

*Après quatre années de négociations législatives, ce règlement a été
définitivement adopté par le Parlement européen le 14 avril 2016. Ses
dispositions sont directement applicables dans l’ensemble des 28 États
membres de l’Union européenne à compter du 25 mai 2018.*

*Ce règlement remplace la directive sur la protection des données
personnelles adoptée en 1995 (article 94 du règlement) ; contrairement
aux directives, les règlements n’impliquent pas que les États membres
adoptent une loi de transposition pour être applicables2.*

*Les principaux objectifs du RGPD sont d’accroître à la fois la
protection des personnes concernées par un traitement de leurs données à
caractère personnel et la responsabilisation des acteurs de ce
traitement. Ces principes pourront être appliqués grâce à l’augmentation
du pouvoir des autorités de régulation.*

-  European Commission :
   https://ec.europa.eu/info/law/law-topic/data-protection_en
-  Wikipedia :
   https://en.wikipedia.org/wiki/General_Data_Protection_Regulation

PROCESS
-------

-  Describe the project
-  Ensure that a clear consent is described and the user clicks on the
   checkbox (sentence is in the active voice + describe with precision
   the kind of treatment)
-  Describe the treatments of personal data
-  Describe the sub contracts such as Microsoft AppInsight, Google
   Analytics, Insurance, Bank

DATA MODEL
----------

How to store the consent?

How to build the audit trail? - application name - which operation has
been triggered? - who performed the operation - list the dossiers that
have been exported (csv format) - Current dateTime - url used to
generate the log

LOG THE CONSENT
~~~~~~~~~~~~~~~

-  application name - user who consent - date of the consent- version of
   the consent - the text of the consent
