.. "=  - # ` : ' " ~ ^ _ *  < >"

Scraping: best practices
-------------------------------


.. graphviz:: _ALT-F1-02-02-TechnoDev-Web_Scraping-Best_practices.dot


Extraordinary examples
------------------------


How To Scrape Amazon Product Data and Prices using Python 3 

Source : https://www.scrapehero.com/tutorial-how-to-scrape-amazon-product-details-using-python-and-selectorlib/


selectors.yml
#############

.. code:: yaml

    name:
        css: '#productTitle'
        type: Text
    price:
        css: '#price_inside_buybox'
        type: Text
    short_description:
        css: '#featurebullets_feature_div'
        type: Text
    images:
        css: '.imgTagWrapper img'
        type: Attribute
        attribute: data-a-dynamic-image
    rating:
        css: span.arp-rating-out-of-text
        type: Text
    number_of_reviews:
        css: 'a.a-link-normal h2'
        type: Text
    variants:
        css: 'form.a-section li'
        multiple: true
        type: Text
        children:
            name:
                css: ""
                type: Attribute
                attribute: title
            asin:
                css: ""
                type: Attribute
                attribute: data-defaultasin
    product_description:
        css: '#productDescription'
        type: Text
    sales_rank:
        css: 'li#SalesRank'
        type: Text
    link_to_all_reviews:
        css: 'div.card-padding a.a-link-emphasis'
        type: Link

Amazon.py
#############

.. code:: python

    from selectorlib import Extractor
    import requests 
    import json 
    from time import sleep


    # Create an Extractor by reading from the YAML file
    e = Extractor.from_yaml_file('selectors.yml')

    def scrape(url):    
        headers = {
            'authority': 'www.amazon.com',
            'pragma': 'no-cache',
            'cache-control': 'no-cache',
            'dnt': '1',
            'upgrade-insecure-requests': '1',
            'user-agent': 'Mozilla/5.0 (X11; CrOS x86_64 8172.45.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.64 Safari/537.36',
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'sec-fetch-site': 'none',
            'sec-fetch-mode': 'navigate',
            'sec-fetch-dest': 'document',
            'accept-language': 'en-GB,en-US;q=0.9,en;q=0.8',
        }

        # Download the page using requests
        print("Downloading %s"%url)
        r = requests.get(url, headers=headers)
        # Simple check to check if page was blocked (Usually 503)
        if r.status_code > 500:
            if "To discuss automated access to Amazon data please contact" in r.text:
                print("Page %s was blocked by Amazon. Please try using better proxies\n"%url)
            else:
                print("Page %s must have been blocked by Amazon as the status code was %d"%(url,r.status_code))
            return None
        # Pass the HTML of the page and create 
        return e.extract(r.text)

    # product_data = []
    with open("urls.txt",'r') as urllist, open('output.jsonl','w') as outfile:
        for url in urllist.readlines():
            data = scrape(url) 
            if data:
                json.dump(data,outfile)
                outfile.write("\n")
                # sleep(5)