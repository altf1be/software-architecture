.. "=  - # ` : ' " ~ ^ _ *  < >"

netflix: architecture
======================

- interface
- catalogue
- serveur

Hardware
--------

- seedbox
    - https://en.wikipedia.org/wiki/Seedbox

Software
---------


- Radaar : automate the catalog of movies

- Sonarr : automate the catalog of tv series

- trakt : follow movies and tv series
    - send teh files to transmission when it is found

- plex : concentrate the medium
    - add metadata
        - movies
        - actors
        - director
        - trailer
        - bonus
        - subtitles
    - enable the view on any device

Operations
----------

1. Add the movie in trakt
2. Radarr searches the movie
3. transmission downloads the movie
4. plex gives the interface
5. plex sends notifications to the phone when the movie is available


URLs
-----

Radarr : https://radarr.video
Sonarr : https://sonarr.tv
Plex : https://www.plex.tv
Trakt : https://trakt.tv/