.. "=  - # ` : ' " ~ ^ _ *  < >"

.NET Curriculum
------------------

Documentation
################

* sphinx : https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html

   1. install sphinx

   2. install template rtfd

.. todo:: 
   3. create a public repo on github presenting a template for sphinx projects

Testing
################

* Creating Unit Tests for ASP.NET MVC Applications (C#) :

   * `<https://docs.microsoft.com/en-us/aspnet/mvc/overview/older-versions-1/unit-testing/creating-unit-tests-for-asp-net-mvc-applications-cs>`_

* Testing and debuggin ASP.NET Web API : 

   * `Testing and Debugging ASP.NET Web API`_

* SeleniumHQ Browser automation : 

   * https://www.seleniumhq.org

* End to end testing angular : 

   * https://www.protractortest.org

Software engineering
####################

- `The Joel Test - 12 Steps to Better Code`_

- `The Joel Test For Programmers (The Simple Programmer Test)`_

Software development .NET
############################

* Microsoft `.NET <https://www.microsoft.com/net>`__ : C#

* Debugging code back-end (Visual Studio)

* Debugging code front-end (Browser, Visual Studio)

* Debugging code on Azure (Browser, Visual Studio)

* MVC : `ASP.NET MVC <https://www.asp.net/mvc>`__ Model-View-Controller

* CSS : https://developer.mozilla.org/en-US/docs/Web/CSS

* Bootstrap 4 : https://getbootstrap.com/

* Authentication/Authorization: `.net identity <https://docs.microsoft.com/en-us/aspnet/core/security/authentication/identity?view=aspnetcore-2.1&tabs=visual-studio>`__

* REST API: securization by token containing a UUID + Claims. Log using MS Account -> Get VSTS tokens -> display Personal projects

* REST API (documentation/deployment): `SWAGGER <https://swagger.io/>`__

* REST API (test) : `POSTMAN <https://www.getpostman.com/>`__

* SPA Single Page Application

   * ANGULAR

      * `Linter <https://en.wikipedia.org/wiki/Lint_(software)>`__ : https://angular.io/cli/lint

   * Typescript

      * `Linter <https://en.wikipedia.org/wiki/Lint_(software)>`__ : https://github.com/Microsoft/dtslint

* NUGET: back-end code + database + migration database + INL/Metis repository

DATA MODELING .NET and Azure
##############################

* `Entity framework <https://docs.microsoft.com/en-us/ef/>`__: Code first
* Database:

   * SQL (SQL Server), `Azure SQL Server <https://azure.microsoft.com/en-us/services/sql-database/>`__ classic (do not use elastic pool)
   * NoSQL: `CosmosDB <https://docs.microsoft.com/en-us/azure/cosmos-db/>`__
* `File storage <https://azure.microsoft.com/en-us/services/storage/files/>`__ (SMB 3.0)

* Media (photo, video, sound): `Azure Media services <https://azure.microsoft.com/en-us/services/media-services/>`__

GENERATE OFFICE FILES
#####################

* File Format APIs : https://www.aspose.com

DEPLOYMENT
##################

* Team Services: `Understand the agile methodology <https://www.visualstudio.com/learn/scale-agile-large-teams>`__

* Azure Web App : https://portal.azure.com

* Azure Pipelines : https://azure.microsoft.com/en-us/services/devops/pipelines

* Jenkins supports building, deploying and automating any project.: https://jenkins.io/

Artificial intelligence / Machine learning / Big data analysis
###################################################################

* Artificial intelligence: https://azure.microsoft.com/en-us/services/cognitive-services

* Machine learning: https://azure.microsoft.com/en-us/services/machine-learning-services

* NTLK Natural Language Toolkit : https://www.nltk.org

Reproducible research 
##############################

* Tools

   * Jupyter notebook are documents produced by the Jupyter Notebook App, which contain both computer code (e.g. python) and rich text elements (paragraph, equations, figures, links, etc): http://jupyter.org

   * nteract and create with data, words, and visuals : https://nteract.io/

   * Microsoft Azure Notebooks : http://notebooks.azure.com

   * Colabotary : https://colab.research.google.com/

* Theory

   * Reproducable research: https://mg.readthedocs.io/reproducible_research.html

   * Reproducibility Workshop: Best practices and easy steps to save time for yourself and other researchers: https://codeocean.com/workshop/caltech