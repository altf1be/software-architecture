.. "=  - # ` : ' " ~ ^ _ *  < >"

Authentic sources
=============================

* Een authentieke bron is een gegevensbank waarin authentieke gegevens worden bewaard en die geldt als dé referentie voor deze specifieke gegevens over personen en rechtsfeiten. Bron: https://dt.bosa.be/nl/authentieke_bronnen

* Une source authentique est une banque de données au sein de laquelle des données authentiques sont conservées et qui sert de référence pour ces données spécifiques sur des personnes et faits juridiques. Source: https://dt.bosa.be/fr/echange_de_donnees/sources_authentiques



Geography - Countries
-----------------------

* Get information about countries via a RESTful API : https://restcountries.eu