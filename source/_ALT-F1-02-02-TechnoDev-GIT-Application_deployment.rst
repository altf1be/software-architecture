.. "=  - # ` : ' " ~ ^ _ *  < >"

.. include:: ___header.rst


GIT lifecycle
==============================================

Description of how to manage the versions, branches in a git repository as well the operations of the software

How to write relevant commits?

A successful git branching model 
--------------------------------

A successful Git branching model : https://nvie.com/posts/a-successful-git-branching-model/

|gitmodel.png|

GIT : commit conventions
------------------------

source : conventional commits : https://github.com/conventional-commits

Semantic messages: http://seesparkbox.com/foundry/semantic_commit_messages

::

   build
   chore (maintain i.e. updating grunt tasks etc; no production code change)
   ci (continuous integration)
   docs (documentation)
   feat (feature)
   fix (bug fix)
   perf (performance improvements)
   refactor (refactoring production code)
   revert
   style (formatting, missing semi colons, …)
   test (adding missing tests, refactoring tests; no production code change)

* enumeration : `<https://github.com/conventional-changelog/commitlint/tree/master/%40commitlint/config-conventional#type-enum>`_


* Install commitlint : `<https://conventional-changelog.github.io/commitlint/#/>`_

.. raw:: html

   <script src="https://gist.github.com/mutewinter/9648651.js"></script>

Example of commits
##############################

.. raw:: html

   <script src="https://gist.github.com/Abdelkrim/d369b2d5961524d66a9b052767e8c450.js"></script>

Source :
https://github.com/conventional-commits/conventionalcommits.org/commits/master


GIT : how to manage the versions, branches … ?
----------------------------------------------

- **GIT** : Create a branch : [BRANCH-DEV] -- [BRANCH-PARENT]
- **DEV** : Local development on Software engineer machine
- **MERGE GIT** : Merge the [BRANCH-DEV] with the [BRANCH-PARENT]

   - The code is merged into the [BRANCH-DEV]

- **STAGING** : The Software is deployed on the staging environment
- **MERGE GIT** : Merge the [BRANCH-DEV] with the [BRANCH-PARENT]

   - The code is merged into the [BRANCH-PARENT]

- **PROD** : Test the PROD version of the software
- **LIVE** : deploy the PROD version of the software on the PROD server

GIT LFS Large File System
-------------------------

- git commands
- Install git lfs https://git-lfs.github.com
- Locks
   - git lfs lock images/foo.jpg
   - git lfs locks
   - git lfs unlock images/foo.jpg
- git lfs push origin master --all

Create a .gitattributes file
############################################################

:: 

   .. include:: .gitattributes

Commands to add files into the repository, and push the code
############################################################

::

   git lfs install
   git lfs track "*.jpg" --lockable
   git lfs track "*.JPG" --lockable
   git lfs track "*.png" --lockable
   git lfs track "*.zip" --lockable
   git lfs track "*.mp4" --lockable
   git lfs track "*.MP4" --lockable
   git lfs track "*.docx" --lockable
   git lfs track "*.svg" --lockable
   git lfs track "*.gif" --lockable
   git lfs track "*.psd" --lockable
   git lfs track "*.sketch" --lockable
   git lfs track "*.ai" --lockable

   git add "*.jpg" "*.JPG" "*.png" "*.zip" "*.mp4" "*.MP4" "*.docx" "*.svg" "*.gif"

   git lfs ls-files
   git lfs env

   git config lfs.https://inlsprl.visualstudio.com/[ProjectName]/_git/[ProjectName].git/info/lfs.locksverify true
   git push origin master
   git lfs push origin master --all

- https://github.com/git-lfs/git-lfs/wiki/File-Locking
