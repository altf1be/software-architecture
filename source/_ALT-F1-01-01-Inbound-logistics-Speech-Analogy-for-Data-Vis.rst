.. "=  - # ` : ' " ~ ^ _ *  < >"

.. include:: ___header.rst

Speech analogy for Data Vis
============================================

Source : https://classroom.udacity.com/courses/ud507/lessons/3063188874/concepts/30639889250923

Parts of speech are to sentences what visual encoding are to charts

POS::Sentences::visual encodings::charts

- Parts of speech are composed of sentences
- charts are composed of visual encodings applied to data types and combined with relationship between those data

.. note:: 
    data types are continuous or categorical

    dimensions are drawn in 1D, 2D, 3D

    Geographic charts
    
    - choropleth = geographic + color
    - cartogram : geographix + size
    - dotmap : georgraphic + shape


The Lie factor
-----------------

- Lie factor describes the integrity of a graphic. if the lie factor is comprised within [0.95 < lie factor < 1.05] then the graphic representative of the data.

.. math:: \text{Lie factor = }\frac{\text{size fo the effect shown in the graphic}}{\text{size of the effect shown in the data}} [0.95 < lie factor < 1.05]
    :label: Lie factor describes the integarty of a graphic

Separation of the visual elements and the structure of data
--------------------------------------------------------------------

- transform data without changing visual representation
- allow for collaboration across teams

Grammar of the Graphics pipeline
--------------------------------

.. graphviz:: _ALT-F1-01-01-Inbound-logistics-grammar-of-graphics-pipeline_graphviz.dot

A. d3.layout : applies common transformations on predefined chart objects

B. d3.nest : groups data based on particular keys and returns an array of JSON

C. d3.selection.attr : changes a characteristic of an element such as position or fill

D. d3.json : loads a data file and returns an array of Javascript objects

E. d3.selection.append : inserts HTML or SVG elements into a web page

F. d3.scale : converts data to a pixel or color value that can be displayed

Process 
----------

- Pre-attentive processing : https://en.wikipedia.org/wiki/Pre-attentive_processing

- Common chart types and how to choose a chart? https://youtu.be/xD2_AU6atqA

Choosing the right chart
--------------------------------

- Chart chooser tool : http://labs.juiceanalytics.com/chartchooser/index.html

- Graph selection matrix : http://www.perceptualedge.com/articles/misc/Graph_Selection_Matrix.pdf

- Visualization types : https://guides.library.duke.edu/datavis/vis_types

- When to use stacked bar charts ? https://solomonmg.github.io/blog/2014/when-to-use-stacked-barcharts/

- Box plots explained : http://www.physics.csbsju.edu/stats/box2.html

- Selecting the Right Graph for Your Message by Stephen Few : 

    - http://www.storytellingwithdata.com/blog/2013/04/chart-chooser

    - http://www.perceptualedge.com/articles/ie/the_right_graph.pdf

|choosing-a-good-chart-09-1-2000x1700.png|


Choose free tools to draw charts
---------------------------------

- https://dimplejs.org

- https://d3js.org

- https://plot.ly

    - US civilian unemployment : https://plot.ly/~Jay-Oh-eN/1

- https://public.tableau.com/s

- https://rawgraphs.io

- https://observablehq.com

- http://openrefine.org

- https://bl.ocks.org

Reviziting the receipt
------------------------------

Source : https://twitter.com/DataToViz/status/1124752405973782528