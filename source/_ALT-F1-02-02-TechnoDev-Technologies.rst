.. "=  - # ` : ' " ~ ^ _ *  < >"

Technologies
============

www.stoic.com
-------------

http://www.stoic.com (tool generating applications from a spreadsheet) - but as you have said, this won’t work at EADS because they don’t allow javascritps to run on their browsers Technologies used by stoic: https://bitbucket.org/altf1be/software-architecture/wiki/Technologies

how to stoic.com generate an application from a spreadsheet?
------------------------------------------------------------

When I have met the founder of stoic last week he told me they generate the applications in this way:

1. they import the spreadsheet and they try to automatically recognize    the fields (date, name, address, etc.)
2. they store the data into 3 different databases:

   1. http://www.postgresql.org/ - relational database to do basic queries
   2. http://www.mongodb.org/ - nosql database storing all the documents
   3. http://www.elasticsearch.org/ - to search into the 2 previous databases

3. They give the means (widgets) for business users to create rules, user interfaces and perspectives (different views of the same application e.g. calendar view, google maps view….)
4. they run the application on www.nodejs.org
5. They deploy the application on http://www.cloudfoundry.com/

stoic.com business model
------------------------

Their business model is based on SaaS subscriptions; they plan to deliver a package one could deploy on its own servers.

stoic.com competitive advantage
-------------------------------

Stoic founder claims that he can generate an application like a sales force automation or www.stackoverflow.com applications within 3-4 days with business experts

stoic.com uses those Javascript technologies
--------------------------------------------

src : http://stoic.com/stack/#/questions/446

- (http://josscrowcroft.github.com/accounting.js/) accounting.js (currency formatting)
- (http://segmentio.github.com/analytics.js/) Analytics.js (web analytics)
- (http://angularjs.org/) AngularJS (user interface framework)
- (http://angular-ui.github.com/) AngularUI (user interface components)
- (http://jhollingworth.github.com/bootstrap-wysihtml5/) bootstrap-wysihtml5 (rich text editor)
- (https://github.com/substack/node-browserify) Browserify (dependency manager)
- (http://hughsk.github.com/colony/) Colony (graph visualization)
- (http://www.eyecon.ro/bootstrap-colorpicker/) Colorpicker for Bootstrap (color picker)
- (http://www.senchalabs.org/connect/) Connect (middleware framework)
- (http://www.jacksasylum.eu/ContentFlow/) ContentFlow (coverflow)
- (http://code.google.com/p/cookie-js/) Cookie.js (cookie library)
- (http://d3js.org/) D3 (visualization library)
- (https://github.com/visionmedia/debug/) debug (logger)
- (https://github.com/eleith/emailjs) emailjs (SMTP client)
- (https://github.com/bnoguchi/everyauth/) everyauth (authentication and authorization package)
- (http://bgrins.github.com/ExpandingTextareas/) Expanding Textareas (expanding textareas)
- (http://expressjs.com/) express (web application framework)
- (https://github.com/cloudhead/eyes.js/tree/) eyes (value inspector)
- (http://fortawesome.github.com/Font-Awesome/) Font Awesome (icon font)
- (http://stoic.com/formula/) Formula.js (formula functions)
- (http://arshaw.com/fullcalendar/) FullCalendar (calendar)
- (https://github.com/jquery/globalize/) Globalize (globalization and localization library)
- (https://github.com/jeff-optimizely/Guiders-JS) Guiders.js (on-page guidance)
- (http://gotwarlost.github.com/istanbul/) istanbul (code coverage tool)
- (http://jade-lang.com/) Jade (template engine)
- (http://silentmatt.com/javascript-expression-evaluator/) JavaScript Expression Evaluator (expression evaluator)
- (http://slexaxton.github.com/Jed/) Jed (internationalization library)
- (http://www.jplayer.org/) jPlayer (media player)
- (http://jquery.com/) jQuery (HTML library)
- (http://masonry.desandro.com/) jQuery Masonry (dynamic layout)
- (http://jquerymobile.com/) jQuery Mobile (mobile user interface library)
- (http://omnipotent.net/jquery.sparkline/) jQuery Sparklines (sparklines)
- (http://jqueryui.com/) jQuery UI (user interface library)
- (http://miller.mageekbox.net/) jQuery.miller.js (miller columns)
- (https://github.com/sgruhier/jquery-addresspicker) jquery-addresspicker (address picker)
- (http://code.google.com/p/jquery-ui-map/) jquery-ui-map (maps)
- (http://jsdox.org) jsdox (API documentation generator)
- (https://github.com/jshint/jshint/) JSHint (code analysis tool)
- (http://jsoneditoronline.org/) JSON Editor Online (JSON editor)
- (http://jstat.org/) jStat (statistics library)
- (http://learnboost.github.com/kue/) Kue (job queue)
- (http://lesscss.org/) LESS (dynamic stylesheet compiler)
- (http://lodash.com/) Lo-Dash (functional programming library)
- (http://github.com/sutoiku/mapperjs/) mapper.js (object data mapper)
- (http://github.com/chjj/marked) marked (markdown compiler)
- (http://digitalbush.com/projects/masked-input-plugin/) Masked Input (input mask library)
- (http://masonry.desandro.com/) Masonry (dynamic layout)
- (http://www.mathjax.org/) MathJax (mathematics display engine)
- (https://github.com/mikejihbe/metrics) Metrics (metrics library)
- (http://visionmedia.github.com/mocha/) mocha (test framework)
- (http://modernizr.com/) Modernizr (feature detection)
- (http://momentjs.com/) Moment.js (date library)
- (http://josscrowcroft.github.com/money.js/) money.js (currency conversion)
- (https://github.com/brianc/node-postgres/) node-postgres (PostgreSQL client)
- (https://github.com/broofa/node-uuid/) node-uuid (UUID generator)
- (https://github.com/C2FO/nools) Nools (business rules engine)
- (http://numeraljs.com/) Numeral.js (number library)
- (http://numericjs.com/) Numeric Javascript (numeric library)
- (http://parseqjs.com) parseq.js (control-flow library)
- (http://phantomjs.org/) PhantomJS (headless WebKit)
- (http://code.shutterstock.com/rickshaw/) Rickshaw (time series graphs)
- (https://github.com/visionmedia/should.js/tree/) should (assertion library)
- (https://github.com/mleibman/SlickGrid) SlickGrid (grid)
- (http://socket.io/) Socket.IO (socket library)
- (https://github.com/fschaefer/Stately.js) Stately.js (finite state machine)
- (http://vojtajina.github.com/testacular/) Testacular (test runner)
- (http://timeline.verite.co/) TimelineJS (timeline)
- (http://twitter.github.com/bootstrap/) Twitter Bootstrap (HTML5 toolkit)
- (https://github.com/mishoo/UglifyJS/) UglifyJS (parser, compressor, beautifier)
- (http://epeli.github.com/underscore.string/) Underscore.string (string library)
- (http://vexflow.com/) VexFlow (music notation rendering)
- (https://github.com/ryanmcgrath/wrench-js) wrench.js (recursive file operations library)