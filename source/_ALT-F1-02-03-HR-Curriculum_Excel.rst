.. "=  - # ` : ' " ~ ^ _ *  < >"

Excel Curriculum
---------------------

Lookups
##########

- Introduction
- VLOOKUP
- VLOOKUP Exact Match
- HLOOKUP
- HLOOKUP Exact Match

Conditional Logic
#####################

- Introduction
- IF Statement
- Nested IF
- AND
- OR
- NOT
- IFERROR
- SUMIF
- AVERAGEIF	
- COUNTIF & COUNTIF
- SUMIFS
- AVERAGEIFS

Data Tools
#############

- Introduction
- Data Validation
- Drop-Down Lists
- Removing Duplicates
- Text To Columns
- Goal Seek
- Scenario Manager

PivotTables
##############

- Introduction
- Creating PivotTables
- Choosing Fields
- PivotTable Layout
- Filtering PivotTables
- Modifying PivotTable Data
- PivotCharts

Collaboration
###################

- Introduction
- Document Properties
- Inserting Hyperlinks
- Sharing a Workbook
- Track Changes
- Accept/Reject Changes
- Mark as Final


Printing
#############

- Introduction
- Page Orientation
- Page Breaks
- Print Area
- Margins
- Print Titles
- Headers and Footers
- Scaling
- Sheet Options

Macros
#######

- Introduction and Macro Security
- Recording a Macro
- Assign a Macro to a Button or Shape
- Run a Macro upon Opening a Workbook
- How to Inspect and Modify a Macro