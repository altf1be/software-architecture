.. "=  - # ` : ' " ~ ^ _ *  < >"

Software engineering
====================

.. note:: `The Joel Test - 12 Steps to Better Code`_

1. Do you use source control?
2. Can you make a build in one step?
3. Do you make daily builds?
4. Do you have a bug database?
5. Do you fix bugs before writing new code?
6. Do you have an up-to-date schedule?
7. Do you have a spec?
8. Do programmers have quiet working conditions?
9. Do you use the best tools money can buy?
10. Do you have testers?
11. Do new candidates write code during their interview?
12. Do you do hallway usability testing?

.. note:: `The Simple Programmer Test`_

1. Can you use source control effectively?
2. Can you solve algorithm-type problems?
3. Can you program in more than one language or technology?
4. Do you do something to increase your education or skills every day?
5. Do you name things appropriately?
6. Can you communicate your ideas effectively?
7. Do you understand basic design patterns?
8. Do you know how to debug effectively?
9. Do you test your own code?
10. Do you share your knowledge?
11. Do you use the best tools for your job?
12. Can you build an actual application?