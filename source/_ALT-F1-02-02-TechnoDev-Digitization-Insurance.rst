.. "= - # ` : ' " ~ ^ _ *  < >"

Digitization for the Insurance industry
------------------------------------------

.. graphviz:: _ALT-F1-02-02-TechnoDev-Digitization-Insurance_graphviz.dot

Presentation layer I
##########################################

.. graphviz:: _ALT-F1-02-02-TechnoDev-Digitization-Insurance_presentation_layer_1_graphviz.dot

The presentation layer is the layer that is in connection with your customers through several means or devices : 

Telephone
`````````````````
A service desk supporting the requests on the phone

Robo advisor
`````````````````
A :term:`Robo advisor` *is a class of financial adviser that provide financial advice or Investment management online with moderate to minimal human intervention*

* The :term:`Robo advisor` scans and dematerializes your documents

* The :term:`Robo advisor` compares the available offers and optimize the portfolio of your customers

* The customer interacts with the trusted Robo advisors via the web, mobile chat or email

Mobile or Smartphone
``````````````````````````````````
Mobile phones accessing the insurer' services through SMS, email, voice of mobile app means

SMS
`````````````````

The customer interacts with the Insurer using SMS. The answers to the SMS are made by a :term:`Robo advisor`, a :term:`Chatbot` or a human. 
The help desks uses an IT system and (s)he is not necessarily answering using a mobile phone.

Web Browser
`````````````````

a software used to access the Web site of the insurer or the :term:`Underwriter` (i.e. Firefox, Opera, Ecosia, Microsoft Internet Explorer, Google Chrome, Microsoft Edge, Safari)

Chatbot
`````````````````

The :term:`Chatbot` is a piece of software that conducts a conversation via auditory or textual methods.

The :term:`Chatbot` tries to answer customers' questions that human would have had as a conversation. Some chatbots use sophisticated natural language processing systems but are most of time supported by a service desk run by humans when the :term:`Chatbot` can't understand the demands made by the customer.

|ALT-F1-Bot-Screenshot_400x711.png|

Presentation layer II
#######################

.. graphviz:: _ALT-F1-02-02-TechnoDev-Digitization-Insurance_presentation_layer_2_graphviz.dot

The presentation layer counts the front office as well as the back office. 
The :term:`Back office` *is all the resources of the company that are devoted to actually producing a product or service and all the other labor that isn't seen by customers, such as administration or logistics*.

Back office workstation
``````````````````````````````````

Those are the collaborators managing the operations ensuring the correct exection of the processes : 

* quote an offer
* validate personal data
* validate the filling of a form
* perform the dunning services duties

The back office requires documentation, software and reports to perform their duties.


Middle office workstation
``````````````````````````````````

The :term:`Middle office` *is made up of the risk managers and the information technology managers who manage risk and maintain the information resources.*

* Track claim settlement times
* Customer satisfation ratings
* Long-term trends in customer activity

Data collected during the operations are stored into IT systems operated by a multitude of managers (risks, operations, HR, Marketing)

Those data are analysed and supports the value chain (logistics, operations, marketing, sales, support) by giving a broad and exact view of the financial situation of the company. 
    
After a careful analysis, the data are shared with the back office who can act and interact with prospects, customers and suppliers depending on the situation (dunning service, quotation, billing, closing off the contract)

.. tip::

    Try the `Insurance claims analysis`_ dashboard

|policy-claim-detailed-analysis.png|

Call centre
`````````````````

Three types of call centres might be operated by a financial service company:

* An :term:`Inbound call center` is operated by a company to administer incoming product or service support or information enquiries from consumers. 
* An :term:`Outbound call center` is operated for telemarketing, for solicitation of charitable or political donations, debt collection, market research, emergency notifications, and urgent/critical needs blood banks. 
* A :term:`Contact center`, further extension to call centers administers centralized handling of individual communications, including letters, faxes, live support software, social media, instant message, and e-mail.

Customer portal
``````````````````````````````````

A website accessible through a Web browser or a mobile phone enabling the customer to access all the aspects of his duties and rights towards the insurer. 

The portal gives access to diverse functionalities:

* Information platform: share details about the products and services, how to contact the insurer
* Transaction platform: create, update or delete information, stop a current insurance, pay electronically the remaining Bills
* Sales platform: generate up-sell and cross-sell opportunities, promote the :term:`Robo advisor` capabilities

* Rewards platform: Insurers retain their customers through the Perceived Value of the customer, the Affinity that the customer has with his insurer, and the Barriers to Exit

    * Perceived value: does the customer feels that (s)he has coverage at a competitive and fair price?
    * Affinity: do the customer has a emotional connection with the customer? Insurance products may tend to have a limited value due to the commoditized nature of the product
    * Barriers to Exit: does the customer has strong and effective reasons to do not leave an insurer? the lack of competition, the increase of costs, the loss of a unique protection, a decrease of the quality of service


Broker portal
`````````````````

A broker portal is a website enabling the :term:`Broker` to perform her/his duties

* Information platform: share details about the products and services, how to contact the insurer, the customers
* Sales platform: support the sales process (from a quote to a signed contract), generate up-sell and cross-sell opportunities
* Marketing platform: identify new sales opportunites by advertising the products and identify the most profitable or potential prospects
* CRM platform: maintain data related to the prospects and customers (contact details, online and offline interactions)
* Dunning service platform: inform and give the tools to enable the broker to run after unpaid invoices till the termination of the contract

Business logic tier I
##########################################

.. graphviz:: _ALT-F1-02-02-TechnoDev-Digitization-Insurance_business_logic_tier_layer_1_graphviz.dot

API Gateway
`````````````````

The :abbr:`API (Application Programming Interface)` describes the functions or the interfaces available between a client and a server. 

APIs are enablers of the platform economy, and allow users to enhance and add services over existing products. 

For example: An API enables an application 'A' to query a system 'B' and collects the schedule of the public transportation (See https://opendata.stib-mivb.be/store/data)

.. tip::
    Look at the description of the :abbr:`API (Application Programming Interface)` from a dunning Service 
    https://dunningcashflow-api.azurewebsites.net/swagger/index.html

|ALT-F1-Dunning_service-Swagger-API-Documentation.PNG|

Business logic tier II
##########################################

.. graphviz:: _ALT-F1-02-02-TechnoDev-Digitization-Insurance_business_logic_tier_layer_2_graphviz.dot

Business Risk Management
``````````````````````````````````
The financial services must comply with a multitude of risks. 

Here are a list of pure risks (loss or no loss only) that an insurer or a :term:`Underwriter` may be confronted with:

* Regulatory Compliance: Invoice compliance, MiFID ii, `MiFID 2`_, Solvency II, `Solvency 2`_
* Tax Compliance: Tax determination, Fiscal reporting, VAT reporting
* Liability risk exposure: product liability risks, or contractual liability risks
* Operational risk: mistakes in process and procedure
* Intellectual property violation risk
* Mortality and morbidity risk at the societal and global level

.. warning::
    * Speculative risks are not described in this documents. i.e. market risk, reputational risk, brand risk, product success risk...

Business Process Management
``````````````````````````````````

The :term:`Business Process Management` *is a discipline aimed at managing all aspect of the business processes; from process design to modeling and analysis to execution and improvement*. 

.. note::
    Here is the description of a process: Data entry of a claim
    
.. graphviz:: _ALT-F1-02-02-TechnoDev-Digitization-Insurance_process-claim_data_entry_graphviz.dot

Business logic tier III
##########################################

.. graphviz:: _ALT-F1-02-02-TechnoDev-Digitization-Insurance_business_logic_tier_layer_3_graphviz.dot

Finance management
``````````````````````````````````

Customer management
``````````````````````````````````

Claims management
``````````````````````````````````

Product management
``````````````````````````````````

Policy management
``````````````````````````````````

.. todo: fill The policy manage

Business logic tier IV
##########################################

.. graphviz:: _ALT-F1-02-02-TechnoDev-Digitization-Insurance_business_logic_tier_layer_4_graphviz.dot

Customers
``````````````````````````````````

Claims
``````````````````````````````````

Products
``````````````````````````````````

Policies
``````````````````````````````````

Entreprise services
``````````````````````````````````

Quotes
``````````````````````````````````

Data tier I
################################

.. graphviz:: _ALT-F1-02-02-TechnoDev-Digitization-Insurance_data_tier_1_graphviz.dot

CDI (Customer Data Integration)
```````````````````````````````````````````````````
:abbr:`CDI (Customer Data Integration)` 

CRM (Customer Relationship Management)
```````````````````````````````````````````````````
:abbr:`CRM (Customer Relationship Management)`

Products catalog
```````````````````````````````````````````````````

Money collection
```````````````````````````````````````````````````

Bills 
```````````````````````````````````````````````````

Underwritings
```````````````````````````````````````````````````

Policies
```````````````````````````````````````````````````

Claims
```````````````````````````````````````````````````

Physical tier
##########################################

.. graphviz:: _ALT-F1-02-02-TechnoDev-Digitization-Insurance_physical_tier_1_graphviz.dot

Data Centers
```````````````````````````````````````````````````

Data storage
```````````````````````````````````````````````````

Servers
```````````````````````````````````````````````````
