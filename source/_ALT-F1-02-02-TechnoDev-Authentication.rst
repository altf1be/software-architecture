.. "=  - # ` : ' " ~ ^ _ *  < >"

Open Authentication (OAuth)
==============================================

*OAuth is an open standard for access delegation, commonly used as a way for Internet users to grant websites or applications access to their information on other websites but without giving them the passwords*.

Source: Wikipedia contributors. (2019, March 19). OAuth. In Wikipedia, The Free Encyclopedia. Retrieved 12:20, March 23, 2019, from https://en.wikipedia.org/w/index.php?title=OAuth&oldid=888559139

Use case for Open Authentication
--------------------------------

A user requires access to a resource on a web application (eg StratEx) using her credentials from another website (eg Microsoft Office365).

1. She needs to login using the form from Office365
2. Office365 will generate a token
3. The token is used by StratEx ensuring that the user is effectively logged using her Office365 credentials
4. StratEx can use the resources made available by Office365 such as username, firstname, lastname, email address, read access to OneDrive, write and send new emails...

Open authentication using Office365
-----------------------------------

Microsoft graph documentation makes available Office365 resources of each registrered user:

- `<https://developer.microsoft.com/en-us/graph>`_
- `<https://docs.microsoft.com/en-us/graph/overview>`_

Request an access token to Office 365: 

- `https://login.microsoftonline.com/common/oauth2/v2.0/authorize?client_id=f5d835b0-4bc1-98e7-f98cb4aaef31&scope=https%3A%2F%2Fgraph.microsoft.com%2Fuser.read&response_type=code&redirect_uri=https%3A%2F%2Ftimesheet-stg-inlsprl.azurewebsites.net%2Fsignin-microsoft&state=Ao8m01yi1E76wQIXPJW-F92Fq1v <https://login.microsoftonline.com/common/oauth2/v2.0/authorize?client_id=f5d835b0-4bc1-98e7-f98cb4aaef31&scope=https%3A%2F%2Fgraph.microsoft.com%2Fuser.read&response_type=code&redirect_uri=https%3A%2F%2Ftimesheet-stg-inlsprl.azurewebsites.net%2Fsignin-microsoft&state=Ao8m01yi1E76wQIXPJW-F92Fq1v>`_


Description of each parameter (see `use-the-authorization-code-to-request-an-access-token`_):

- https://login.microsoftonline.com/common/oauth2/v2.0/**authorize**
- ? **client_id** =f5d835b0-4bc1-98e7-f98cb4aaef31
- & **scope** =https%3A%2F%2Fgraph.microsoft.com%2Fuser.read
- & **response_type** =code
- & **redirect_uri** =https%3A%2F%2Ftimesheet-stg-inlsprl.azurewebsites.net%2Fsignin-microsoft
- & **state** =Ao8m01yi1E76wQIXPJW-F92Fq1v