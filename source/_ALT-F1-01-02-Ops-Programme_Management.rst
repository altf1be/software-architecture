.. "=  - # ` : ' " ~ ^ _ *  < >"

Programme Management
============================================

Program Manager Responsibilities

* Organizing programs and activities in accordance with the mission and goals of the organization.
* Developing new programs to support the strategic direction of the organization.
* Creating and managing long-term goals.
* Developing a budget and operating plan for the program.
* Developing an evaluation method to assess program strengths and identify areas for improvement.
* Writing program funding proposals to guarantee uninterrupted delivery of services.
* Managing a team with a diverse array of talents and responsibilities.
* Ensuring goals are met in areas including customer satisfaction, safety, quality, and team member performance.
* Implementing and managing changes and interventions to ensure project goals are achieved.
* Meeting with stakeholders to make communication easy and transparent regarding project issues and decisions on services.
* Producing accurate and timely reporting of program status throughout its life cycle.
* Analyzing program risks.
* Working on strategy with the marketing team.

source: `Program Manager Job Description: <https://www.betterteam.com/program-manager-job-description>`_

.. uml:: _ALT-F1-01-02-Ops-Programme_Management.plantuml
