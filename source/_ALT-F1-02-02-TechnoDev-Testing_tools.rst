.. "=  - # ` : ' " ~ ^ _ *  < >"

ngrok - Public URLs for exposing your local web server
======================================================

Use ngrok to grant access to your localhost to anyone

1. Install https://ngrok.com/download

2. open the port where the web server is located. run the following command :

    1. **[path to ngrok]**\\ngrok.exe http **[port to open on your localhost]** -host-header=rewrite

3. share the URL to the person who needs to access your local machine. i.e. https://a1cc816e.ngrok.io

::

    ngrok by @inconshreveable

    Session Status online Account [the account name] (Plan: Free) Update update available (version 2.2.8, Ctrl-U to update) Version 2.2.3
    Region United States (us) Web Interface http://127.0.0.1:4040
    Forwarding http://a1cc816e.ngrok.io -> localhost:4624 Forwarding
    
    https://a1cc816e.ngrok.io -> localhost:4624