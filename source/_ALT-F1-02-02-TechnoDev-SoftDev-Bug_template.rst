.. "=  - # ` : ' " ~ ^ _ *  < >"

How to write a bug report?
==========================

1. Copy paste the content hereunder
2. Create a new issue:
   https://bitbucket.org/altf1be/software-architecture/issues/new

::

   ## WHAT STEPS WILL REPRODUCE THE PROBLEM?

   1. Open the page
   2.
   3.

   ## WHAT IS THE EXPECTED OUTPUT? 

   *  StratEx is loaded


   ## WHAT DO YOU SEE INSTEAD?

   * The screenshot attached to this email
   * StratEx cannot be opened because of a problem

   ## WHAT VERSION OF THE PRODUCT ARE YOU USING? 

   * Version: 3.5.6245.20028
   * on [https://www.stratexapp.com](https://www.stratexapp.com)
   * on [https://staging.stratexapp.com](https://staging.stratexapp.com)
   * on [https://develop.stratexapp.com](https://develop.stratexapp.com)


   ## ON WHAT OPERATING SYSTEM, BROWSER, ETC.? 

   * Windows 7.1
      * Chrome Version 54
      * Internet Explorer 11
      * Opera Version 41
   * Windows 10
      * Internet Explorer
      * Edge
   * Mac OS X 10.9 (13A603)
      * Safari Version 7.0 (9537.71)
      * Chrome Version 31.0.1650.57

   ## PLEASE PROVIDE ANY ADDITIONAL INFORMATION BELOW.

   * None
   * Extra files are available on [StratExApp files on Google Drive]
   * Find the private [Videos generated on GDrive]
   * Find the public [Videos on StratEx YouTube channel]
   * Find the public [Documentation on Read The Docs]

   ## Bug report (if any)

   * None

   [StratExApp files on Google Drive]: https://drive.google.com/a/alt-f1.be/folderview?id=0B9L2cx0TUjLGUFZBSkF6WlFCYms&usp=sharing#list
   [Videos generated on GDrive]: https://drive.google.com/a/alt-f1.be/folderview?id=0B9L2cx0TUjLGa190N1ZURHBpUFE&usp=sharing
   [Videos on StratEx YouTube channel]: https://www.youtube.com/channel/UCuwGfoVoozq0ZTmHJ3WCvTQ
   [Documentation on Read The Docs]: http://stratexapp-docs.readthedocs.org/en/latest/