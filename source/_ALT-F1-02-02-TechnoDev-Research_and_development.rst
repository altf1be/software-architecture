.. "=  - # ` : ' " ~ ^ _ *  < >"

Research & Development topics
=============================

0. Prerequisite:

   - Use Technologies supported by Microsoft

      - ASP.NET - https://docs.microsoft.com/en-us/aspnet/overview
      - ASP.NET Core - https://docs.microsoft.com/en-us/aspnet/core/
      - Microsoft Azure: https://portal.azure.com

1. Single Page Application (SPA)

   - Build a SPA such as Microsoft Azure for our customers
   - Test SPA App

2. IT Automation best practices

   - Reading : https://www.joelonsoftware.com/2000/08/09/the-joel-test-12-steps-to-better-code/
   - In general we should go for 1-click test/deployment
   - Design
   - Development
   - Test (SPA, Web apps)

      - Unit
      - UAT (user acceptance test)

   - Deployment
   - Documentation

      - Design
      - Code
      - User manual

- Security
