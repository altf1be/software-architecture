.. "=  - # ` : ' " ~ ^ _ *  < >"

.. include:: ___header.rst

Sales automation
=================

- Use Zoho CRM : https://www.zoho.com/crm/

1. Créer des **leads** (nom, prénom, email address, nom de société) pour XYZ digital au minimum
2. Convertissez des **leads** en **Contact** (personne physique travaillant pour une personne morale) et **Account** (personne morale)
3. Ajouter un **deal** lorsque vous convertissez un **lead** en **Contact**
4. Créer des **deals** (projets signés ou en cours)

Sales Dashboard
================

 Baremetrics : https://demo.baremetrics.com

|baremetrics.io-dashboard.png|

Contracts management
====================

- The Service Provider

    - **is** Shareholder of the customer

        - the Service Provider Lowers the price

    - **is not** Shareholder of the customer

        - the Service Provider asks for a regular price

- The contract includes

    - 3rd parties with whom the Service Provider **has no** liabilities towards the 3rd party

        - The Service Provider has NOTHING to do contractually

    - 3rd parties with whom the Service Provider **has** liabilities towards the 3rd party

        - A contract describing the governance MUST be written
