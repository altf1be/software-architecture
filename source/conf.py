# -*- coding: utf-8 -*-

# opiniated conf.py https://dagshub.com/ttu/CaReCur/src/master/docs
#
# Configuration file for the Sphinx documentation builder.
#
# This file does only contain a selection of the most common options. For a
# full list see the documentation:
# http://www.sphinx-doc.org/en/master/config

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import unidecode
from dynaconf import settings
import os
import sys
import datetime
import re
# import sphinx_rtd_theme
sys.path.insert(0, os.path.abspath('.'))

# from . import config_altf1be

# -- Project information -----------------------------------------------------

title = settings.get("title", "My Title")
project_slug = re.sub("([^a-zA-Z0-9])+", "-", title).lower()

project = settings.get("project", "My Project")

author = settings.get("author", "My Author")

copyright = copyright = settings.get(
    "copyright", "") or f"{datetime.today().year}, {author}"


# The short X.Y version
version = u'v' + datetime.datetime.today().strftime('%Y.%m.%d')
# The full version, including alpha/beta/rc tags
release = version + ' ' + datetime.datetime.today().strftime('%H.%M')


# -- General configuration ---------------------------------------------------

# If your documentation needs a minimal Sphinx version, state it here.
#
# needs_sphinx = '1.0'

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'docxbuilder',
    'nbsphinx',
    'recommonmark',
    'sphinx_rtd_theme',
    'sphinx.ext.autodoc',
    'sphinx.ext.coverage',
    'sphinx.ext.doctest',
    'sphinx.ext.githubpages',
    'sphinx.ext.graphviz',
    'sphinx.ext.ifconfig',
    'sphinx.ext.imgmath',
    'sphinx.ext.intersphinx',
    'sphinx.ext.mathjax',
    'sphinx.ext.napoleon',
    'sphinx.ext.todo',
    'sphinx.ext.viewcode',
    'sphinxcontrib.bibtex',
    'sphinxcontrib.plantuml',
    # 'plantweb.directive',
]

# TODO: Add in other extensions in the toml file
# Pseudo-code:
extensions.extend(filter(lambda ext: ext not in extensions,
                         settings.get("extensions")))

# -- Figure and Caption Settings -----

# See: https://www.sphinx-doc.org/en/master/usage/configuration.html#confval-numfig
numfig = True


# The master toctree document.
master_doc = 'index'


####################################################################################
# plantuml https://sourceforge.net/projects/plantuml/files/plantuml.jar/download

cwd = os.getcwd()
plantuml = 'java -jar %s ' % os.path.join(
    cwd,
    "utils",
    "plantuml.1.2021.1.jar"
    )

on_rtd = os.environ.get('READTHEDOCS') == 'True'
if on_rtd:
    if not os.path.exists('./git-lfs'):
        os.system('wget https://github.com/git-lfs/git-lfs/releases/download/v2.7.1/git-lfs-linux-amd64-v2.7.1.tar.gz')
        os.system('tar xvfz git-lfs-linux-amd64-v2.7.1.tar.gz')
        os.system('./git-lfs install')  # make lfs available in current repository
        os.system('./git-lfs fetch')  # download content from remote
        os.system('./git-lfs checkout')  # make local files to have the real content on them

    # -DRELATIVE_INCLUDE="C4"
print(f"plantuml: {plantuml}")
# plantuml = ['java', '-jar', r"/usr/share/plantuml/plantuml.jar", "-DRELATIVE_INCLUDE='C4'"] #, "-config plantuml.cfg"]
plantuml_output_format = 'png'
plantuml_latex_output_format = 'png'

####################################################################################
bibtex_bibfiles = [
    # os.path.join(
    #     '_citations', 'COVID-19', '10.4209_aaqr.2020.06.0302.bib'
    # ),

]

####################################################################################
# https://www.Graphviz.org

graphviz_output_format = 'svg'

####################################################################################
# DOCX
docx_documents = [
    (master_doc,  project_slug+'.docx', {
        'title': title,
        'creator': str(author),
        'subject': str(project),
        'created': datetime.datetime.today().strftime('%Y-%m-%d')
    }, True),
]
docx_pagebreak_before_section = 1
docx_update_fields = True

# DOES NOT WORK - docx_style = 'templates/single_spaced.docx'
# DOES NOT WORK - docx_style = 'templates/letterhead.docx'
# docx_style = 'path/to/custom_style.docx'
# DOES NOT WORK - issue with xpath : docx_style = 'templates/tpl-bold_monogram_red.docx'
# docx_style = 'templates/tpl-modern_chronological.docx'

# DOES NOT WORK - docx_style = 'templates/tpl_green.docx'
# DOES NOT WORK - docx_style = 'templates/a_template_orange.docx'
# DOES NOT WORK - docx_style = 'templates/tpl_word.docx'

# docx_style = 'templates/reference-la-boucherie.docx'
# docx_style = 'templates/tf44608798_win32.dotx'
# docx_style = 'templates/tf04021852_win32.dotx'
# docx_style = 'templates/tf78647202_win32.dotx'
# docx_style = 'templates/tf03463080_win32.dotx'
# docx_style = 'templates/w11861-template-word.dotx'

# docx_style = 'templates/education-minutes.docx'
# docx_style = 'templates/apa-style-6th.docx'

# docx_style = '_templates/docx/libreoffice.docx'
# docx_style = '_templates/docx/resume-EN.docx'
# docx_style = '_templates/docx/docxbuilder-style_custom.docx'
docx_style = '_templates/docx/reference-la-boucherie.docx'


####################################################################################
# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# The suffix(es) of source filenames.
# You can specify multiple suffix as a list of string:
#
source_suffix = {
    '.rst': 'restructuredtext',
    # '.md': 'markdown',
}

# The language for content autogenerated by Sphinx. Refer to documentation
# for a list of supported languages.
#
# This is also used if you do content translation via gettext catalogs.
# Usually you set "language" from the command line for these cases.
language = 'en'

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = [
    "_*.rst",
    "CHANGELOG.md"
    "README.md"
    "_common/_*.rst",
    "_common/copyrights/_*.rst",
    "_old",
    "project/_*.rst",
    "secure_and_private_ai/_*.rst",
    "secure_and_private_ai/**/readme.rst",
]


# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'sphinx'


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

# Theme options are theme-specific and customize the look and feel of a theme
# further.  For a list of options available for each theme, see the
# documentation.
#
html_theme_options = {
    #     'canonical_url': '',
    #     'analytics_id': '',
    #     'logo_only': False,
    'display_version': True,
    'prev_next_buttons_location': 'both',
    'style_external_links': True,
    #     #'vcs_pageview_mode': '',
    #     # Toc options
    'collapse_navigation': True,
    #     'sticky_navigation': True,
    'navigation_depth': 4,
    #     'includehidden': True,
    #     'titles_only': False
}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# Custom sidebar templates, must be a dictionary that maps document names
# to template names.
#
# The default sidebars (for documents that don't match any pattern) are
# defined by theme itself.  Builtin themes are using these templates by
# default: ``['localtoc.html', 'relations.html', 'sourcelink.html',
# 'searchbox.html']``.
#
# html_sidebars = {}


# -- Options for HTMLHelp output ---------------------------------------------

# Output file base name for HTML help builder.
htmlhelp_basename = project_slug + 'Doc'


# -- Options for LaTeX output ------------------------------------------------
f = open(os.path.join('_templates', 'latex', 'latex-styling.tex'), 'r+')
PREAMBLE = f.read()

latex_elements = {
    # https://www.sphinx-doc.org/en/master/latex.html
    # 'classoptions': ',openany,twoside',

    'classoptions': ',openany',

    # 'babel' : '\\usepackage[french]{babel}',
    # The paper size ('letterpaper' or 'a4paper').
    #
    'papersize': 'a4paper',

    # The font size ('10pt', '11pt' or '12pt').
    #
    # 'pointsize': '10pt',

    # Additional stuff for the LaTeX preamble.
    #
    # 'preamble': '',
    'preamble': PREAMBLE,
    'maxlistdepth': '10',
    # Latex figure (float) alignment
    #
    # 'figure_align': 'htbp',
    'figure_align': 'H',
}

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title,
#  author, documentclass [howto, manual, or own class]).
latex_documents = [
    (master_doc, f"{project_slug}.tex", title,
     author, 'howto'),
]
latex_logo = settings.get("logo", None)

if settings.get("printed", False):
    latex_show_pagerefs = True
    latex_show_urls = "footnote"

# -- Options for manual page output ------------------------------------------

# One entry per manual page. List of tuples
# (source start file, name, description, authors, manual section).
man_pages = [
    (master_doc, project_slug, title,
     [author], 1)
]


# -- Options for Texinfo output ----------------------------------------------

# Grouping the document tree into Texinfo files. List of tuples
# (source start file, target name, title, author,
#  dir menu entry, description, category)
texinfo_documents = [
    (master_doc, project_slug, title,
     author, project_slug, title,
     'Miscellaneous'),
]


# -- Options for Epub output -------------------------------------------------

# Bibliographic Dublin Core info.
epub_title = project_slug
epub_author = author
epub_publisher = author
epub_copyright = copyright

# The unique identifier of the text. This can be a ISBN number
# or the project homepage.
#
# epub_identifier = ''

# A unique identification for the text.
#
# epub_uid = ''

# A list of files that should not be packed into the epub file.
epub_exclude_files = ['search.html']


# -- Extension configuration -------------------------------------------------

# -- Options for intersphinx extension ---------------------------------------

# Example configuration for intersphinx: refer to the Python standard library.
intersphinx_mapping = {'https://docs.python.org/': None}

# -- Options for todo extension ----------------------------------------------

# If true, `todo` and `todoList` produce output, else they produce nothing.
todo_include_todos = True

# CSS customization


def setup(app):
    app.add_css_file('css/custom.css')
