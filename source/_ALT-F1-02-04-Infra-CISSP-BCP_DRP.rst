.. "=  - # ` : ' " ~ ^ _ *  < >"

CISSP
==============================================

Business Continuity Plan
------------------------

When business is disrupted, it can cost money. Lost revenues plus extra
expenses means reduced profits. Insurance does not cover all costs and
cannot replace customers that defect to the competition. A business
continuity plan to continue business is essential. Development of a
business continuity plan includes four steps:

- Conduct a business impact analysis to identify time-sensitive or critical business functions and processes and the resources that support them.

- Identify, document, and implement to recover critical business functions and processes.

- Organize a business continuity team and compile a business continuity plan to manage a business disruption.

- Conduct training for the business continuity team and testing and exercises to evaluate recovery strategies and the plan.

- Source Official website of the Department of Homeland Security : https://www.ready.gov/business/implementation/continuity

Disaster Recovery Plan
-----------------------

An information technology disaster recovery plan (IT DRP) should be developed in conjunction with the business continuity plan. Priorities and recovery time objectives for information technology should be developed during the business impact analysis. Technology recovery strategies should be developed to restore hardware, applications and data in time to meet the needs of the business recovery.

Source Official website of the Department of Homeland Security : https://www.ready.gov/business/implementation/IT