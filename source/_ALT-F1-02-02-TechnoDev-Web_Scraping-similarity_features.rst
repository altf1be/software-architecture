.. "=  - # ` : ' " ~ ^ _ *  < >"

Similarity features
--------------------------------

:term:`Jaccard`: https://en.wikipedia.org/wiki/Jaccard_index

:term:`TD-IDF`, term frequency–inverse document frequency: https://en.wikipedia.org/wiki/Tf%E2%80%93idf

:term:`Mann–Whitney U test` *is a nonparametric test of the null hypothesis that, for randomly selected values X and Y from two populations, the probability of X being greater than Y is equal to the probability of Y being greater than X.*

:term:`Kolmogorov–Smirnov test` *In statistics, the Kolmogorov–Smirnov test (K–S test or KS test) is a nonparametric test of the equality of continuous (or discontinuous, see Section 2.2), one-dimensional probability distributions that can be used to compare a sample with a reference probability distribution (one-sample K–S test), or to compare two samples (two-sample K–S test).*

.. graphviz:: _ALT-F1-02-02-TechnoDev-Web_Scraping-similarity_features.dot


Source : AAAI 2018 Tutorial Building Knowledge Graphs