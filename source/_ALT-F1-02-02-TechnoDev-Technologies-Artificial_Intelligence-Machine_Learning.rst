Gaspard Koenig : https://fr.wikipedia.org/wiki/Gaspard_Koenig

Vidéo: BFM, la librairie de l'éco: https://bfmbusiness.bfmtv.com/mediaplayer/video/la-librairie-de-l-eco-vendredi-20-septembre-2019-1188416.html

"Gaspard Koenig a interrogé 120 personnes : chercheurs, investisseurs...

les techniques de machine learning et d'intelligence artificielle ne consistent pas à reproduire le processus de la pensée, elles consistent à reproduire le résultat de la pensée

l'IA est nourrie à partir d'énormément de travail humain, d'images labellisées que ce soit pour identifier des chats ou reconnaître des tumeurs sur des scans de  cancers.

donc, le traitement du résultat de la pensée humaine est plus rapide et permet de faire des progrès


Le livre de Nick Bostrom (https://en.wikipedia.org/wiki/Nick_Bostrom) qui a du mal à décrire l'intelligence que par une puissance de calcul 
Bostrom a scindé ce qu'est l'intelligence dans et hors du cerveau et le corps humain qui n'a plus d'intérêt

On n'a jamais indiqué qu'une personne était ou pas intelligence si elle pouvait résoudre une équation du 4ème degré en 20 secondes.

l'intelligence est un ensemble de valeur, de signification et de choses qui sont de l'ordre moral.

c'est bien un être humain qui indiquera à une voiture autonome les choix à effectuer en cas de risque d'accident: doit-on renverser un enfant, une personne âgée ou un animal?"