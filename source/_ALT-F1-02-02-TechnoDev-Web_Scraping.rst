.. "=  - # ` : ' " ~ ^ _ *  < >"

Web Scraping
=============================

Documentation
-------------

* https://delftswa.gitbooks.io/desosa-2017/content/scrapy/chapter.html, Joren Hammudoglu (@jorenham), Johan Jonasson (@jojona), Marnix de Graaf (@Eauwzeauw)

.. include:: _ALT-F1-02-02-TechnoDev-Web_Scraping-Software_architecture.rst

.. include:: _ALT-F1-02-02-TechnoDev-Web_Scraping-Best_practices.rst

.. include:: _ALT-F1-02-02-TechnoDev-Web_Scraping-data_model.rst 

.. include:: _ALT-F1-02-02-TechnoDev-Web_Scraping-crawlers_scrapy_spiders.rst



.. include:: _ALT-F1-02-02-TechnoDev-Web_Scraping-similarity_features.rst 