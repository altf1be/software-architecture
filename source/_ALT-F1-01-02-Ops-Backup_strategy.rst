.. "=  - # ` : ' " ~ ^ _ *  < >"

Backup
============================================

Which ressources do you need to backup?
---------------------------------------

- Virtual machines running an Operating system (Windows, MacOSX, Linux...)
- Databases (Microsoft SQL Server, MongoDB, Oracle Database, MySQL...)
- File systems (Directories containing documents, images...)

Which strategy do you need to apply?
---------------------------------------

- Store forever one full backup per year from January the 1st (Year Y1, Y2...)

- Store everyday day one incremental backup for each resource

    - Store a full backup the first of each month 
    - Store an incremental backup from Day 2 to the end of the month (31-1 backups)
    - Replace the incremental backup performed one month earlier (Month M-1)
    - Keep the full backup made once a month

Wrap up
-----------

- Store in total :

    - 12 full backups per year for each month (1st day of each month)
    - 31 incremental backups for the last 31 days
    - 1 full backup per year

- Example:

    - Year 1 : 12 + 31 backups
    - Year 2 : 12 + 31 + 1 backups
    - Year 3 : 12 + 31 + 2 backups