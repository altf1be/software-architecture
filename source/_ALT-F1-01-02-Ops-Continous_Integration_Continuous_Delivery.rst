.. "=  - # ` : ' " ~ ^ _ *  < >"

Continous Integration-Continuous Delivery
============================================

|tfs.ci-cd-process.png|

|cicd-CIvsCIvsCD.png|

Continuous Integration
---------------------------

Continuous Integration (CI) is straightforward and stands for continuous integration, a practice that focuses on making preparing a release easier. 
The acronym CD) can either mean continuous delivery or continuous deployment.

source : `Continuous integration vs. continuous delivery vs. continuous deployment`_

.. graphviz:: _ALT-F1-01-02-Ops-Continous_Integration_Continuous_Delivery_graphviz.dot


Continuous Delivery
---------------------------

Continuous delivery (CD) is an extension of continuous integration to make sure that you can release new changes to your customers quickly in a sustainable way.

You automate your release process and deploy your application at any point of time by clicking on a button on a daily, weekly, fortnightly basis.

source : `Continuous integration vs. continuous delivery vs. continuous deployment`_


Continuous Deployment
---------------------------

Continuous deployment (CD) implies that every change that passes all stages of your production pipeline is released to your customers. There's no human intervention, and only a failed test will prevent a new change to be deployed to production.

source : `Continuous integration vs. continuous delivery vs. continuous deployment`_