.. include:: ___header.rst

.. "= - # ` : ' " ~ ^ _ *  < >"


Secure and Private Artificial Intelligence
--------------------------------------------------------------------------------------------------------

* course source : 

    * https://classroom.udacity.com/courses/ud185

    * other course : https://www.udacity.com/school-of-ai


Deep learning with PyTorch
##########################################################################################################

* https://research.fb.com/category/facebook-ai-research
* Notebooks : https://github.com/udacity/deep-learning-v2-pytorch


Install Python3
`````````````````````````````````````````````````````````````````

* create a python3.7.X environment : `conda create -n py37 python=3.7 anaconda`

* activate the environment `conda activate py37`

* deactivate the environment `conda deactivate`

* determining my environment : `conda info --envs`

Install PyTorch an Conda
`````````````````````````````````````````````````````````````````

* Install Conda

    * install Anaconda : https://docs.anaconda.com/anaconda/install and https://conda.io/en/latest

    * or install Miniconda : https://docs.conda.io/en/latest/miniconda.html

    * Some commands

        * managing environments : https://conda.io/projects/conda/en/latest/user-guide/getting-started.html#managing-environments

        * example of commands : `conda search scipy`, `conda install scipy`, `conda build my_fun_package`, `conda update conda`

* Install PyTorch https://pytorch.org/get-started/locally

    * for old GPU (does not work on Geforce GT 520M `conda install pytorch torchvision cudatoolkit=9.0 -c pytorch -c defaults -c numba/label/dev`
    * `conda install pytorch torchvision cudatoolkit=10.0 -c pytorch`

* Install numpy, jupyter and notebook

    * `conda install numpy jupyter notebook`


Launching Jupyter Notebook App
`````````````````````````````````````````````````````````````````

* `jupyter notebook` - https://jupyter-notebook-beginner-guide.readthedocs.io/en/latest/execute.html


Udacity course : Deep Learning with PyTorch
############################################################

This repo contains notebooks and related code for Udacity's Deep Learning with PyTorch lesson. This lesson appears in our [AI Programming with Python Nanodegree program](https://www.udacity.com/course/ai-programming-python-nanodegree--nd089).

* **Part 1:** Introduction to PyTorch and using tensors
* **Part 2:** Building fully-connected neural networks with PyTorch
* **Part 3:** How to train a fully-connected network with backpropagation on MNIST
* **Part 4:** Exercise - train a neural network on Fashion-MNIST
* **Part 5:** Using a trained network for making predictions and validating networks
* **Part 6:** How to save and load trained models
* **Part 7:** Load image data with torchvision, also data augmentation
* **Part 8:** Use transfer learning to train a state-of-the-art image classifier for dogs and cats


Tools for Artificial Intelligence
############################################################

* Gym is a toolkit for developing and comparing reinforcement learning algorithms. It supports teaching agents everything from walking to playing games like Pong or Pinball. https://gym.openai.com

* ONNX is an open format to represent deep learning models. With ONNX, AI developers can more easily move models between state-of-the-art tools and choose the combination that is best for them. ONNX is developed and supported by a community of partners. https://onnx.ai

* Machine learning cheatsheet : https://ml-cheatsheet.readthedocs.io