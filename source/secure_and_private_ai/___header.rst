.. the header contains shared information by documentation pages

.. URL Links

.. _AETA: https://www.aeta-audio.com/en/
.. _Angular: https://angular.io/
.. _Apache HTTP server: _http://httpd.apache.org/
.. _JITSI: https://github.com/jitsi/libjitsi
.. _NGINX: https://www.nginx.com/
.. _nodejs: https://nodejs.org/en/
.. _PHP: https://www.php.net/
.. _SIREMIS: https://siremis.asipto.com

.. Substitutions of Images
