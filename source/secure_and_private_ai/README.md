# Secure and Private AI : e-learning

e-learning from large platforms : https://udacity.com

## Courses

- private ai : https://classroom.udacity.com/courses/ud185


## release as a target type

    git checkout master; git pull origin master
    standard-version --first-release
    standard-version
    
### git add push

    npm run release -- --release-as patch | minor | major 
    git push --follow-tags origin master

## Run Jupyter notebooks

* On windows : `start > Ananconda prompt (miniconda2)`
* activate the environment `conda activiate py37`
* deactivate the environment `conda deactivate`
* determining my environment : `conda info --envs`