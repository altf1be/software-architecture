# read me

## transfer_learning_tutorial.py

Source : https://pytorch.org/tutorials/beginner/transfer_learning_tutorial.html

  (py37) C:\dev\bb\altf1be\software-architecture\source\secure_and_private_ai\py>python transfer_learning_tutorial.py
  Epoch 0/24
  ----------
  train Loss: 0.6048 Acc: 0.6762
  val Loss: 0.2872 Acc: 0.8954

  Epoch 1/24
  ----------
  train Loss: 0.5040 Acc: 0.7623
  val Loss: 0.1766 Acc: 0.9412

  Epoch 2/24
  ----------
  train Loss: 0.5683 Acc: 0.7705
  val Loss: 0.2245 Acc: 0.9281

  Epoch 3/24
  ----------
  train Loss: 0.5957 Acc: 0.7582
  val Loss: 0.2468 Acc: 0.9150

  Epoch 4/24
  ----------
  train Loss: 0.4215 Acc: 0.8320
  val Loss: 0.3011 Acc: 0.9216

  Epoch 5/24
  ----------
  train Loss: 0.5306 Acc: 0.8238
  val Loss: 0.5068 Acc: 0.8562

  Epoch 6/24
  ----------
  train Loss: 0.3275 Acc: 0.8607
  val Loss: 0.4621 Acc: 0.8366

  Epoch 7/24
  ----------
  train Loss: 0.3282 Acc: 0.8566
  val Loss: 0.2591 Acc: 0.9020

  Epoch 8/24
  ----------
  train Loss: 0.2767 Acc: 0.8770
  val Loss: 0.2780 Acc: 0.8889

  Epoch 9/24
  ----------
  train Loss: 0.2781 Acc: 0.8934
  val Loss: 0.2828 Acc: 0.9150

  Epoch 10/24
  ----------
  train Loss: 0.2592 Acc: 0.8893
  val Loss: 0.2287 Acc: 0.9477

  Epoch 11/24
  ----------
  train Loss: 0.2731 Acc: 0.8811
  val Loss: 0.2554 Acc: 0.9150

  Epoch 12/24
  ----------
  train Loss: 0.2562 Acc: 0.8934
  val Loss: 0.2291 Acc: 0.9216

  Epoch 13/24
  ----------
  train Loss: 0.3080 Acc: 0.8566
  val Loss: 0.2258 Acc: 0.9346

  Epoch 14/24
  ----------
  train Loss: 0.2046 Acc: 0.9098
  val Loss: 0.2292 Acc: 0.9412

  Epoch 15/24
  ----------
  train Loss: 0.2670 Acc: 0.8934
  val Loss: 0.2163 Acc: 0.9346

  Epoch 16/24
  ----------
  train Loss: 0.2814 Acc: 0.8770
  val Loss: 0.2453 Acc: 0.9150

  Epoch 17/24
  ----------
  train Loss: 0.2407 Acc: 0.8893
  val Loss: 0.2331 Acc: 0.9216

  Epoch 18/24
  ----------
  train Loss: 0.3153 Acc: 0.8648
  val Loss: 0.2298 Acc: 0.9216

  Epoch 19/24
  ----------
  train Loss: 0.2576 Acc: 0.8770
  val Loss: 0.2448 Acc: 0.9281

  Epoch 20/24
  ----------
  train Loss: 0.2477 Acc: 0.8893
  val Loss: 0.2392 Acc: 0.9216

  Epoch 21/24
  ----------
  train Loss: 0.2363 Acc: 0.9139
  val Loss: 0.2402 Acc: 0.9346

  Epoch 22/24
  ----------
  train Loss: 0.2810 Acc: 0.8770
  val Loss: 0.2212 Acc: 0.9477

  Epoch 23/24
  ----------
  train Loss: 0.2639 Acc: 0.8811
  val Loss: 0.2341 Acc: 0.9346

  Epoch 24/24
  ----------
  train Loss: 0.2977 Acc: 0.9016
  val Loss: 0.2375 Acc: 0.9085

  Training complete in 72m 58s
  Best val Acc: 0.947712

Epoch 0/24
----------
train Loss: 0.6302 Acc: 0.6762
val Loss: 0.3275 Acc: 0.8497

Epoch 1/24
----------
train Loss: 0.3972 Acc: 0.8156
val Loss: 0.1671 Acc: 0.9608

Epoch 2/24
----------
train Loss: 0.6888 Acc: 0.7336
val Loss: 0.2058 Acc: 0.9281

Epoch 3/24
----------
train Loss: 0.4571 Acc: 0.8115
val Loss: 0.1702 Acc: 0.9412

Epoch 4/24
----------
train Loss: 0.4083 Acc: 0.8115
val Loss: 0.1604 Acc: 0.9542

Epoch 5/24
----------
train Loss: 0.3540 Acc: 0.8689
val Loss: 0.4632 Acc: 0.8105

Epoch 6/24
----------
train Loss: 0.4872 Acc: 0.7705
val Loss: 0.2104 Acc: 0.9346

Epoch 7/24
----------
train Loss: 0.4146 Acc: 0.8238
val Loss: 0.1705 Acc: 0.9542

Epoch 8/24
----------
train Loss: 0.3527 Acc: 0.8484
val Loss: 0.1586 Acc: 0.9477

Epoch 9/24
----------
train Loss: 0.4173 Acc: 0.8279
val Loss: 0.1723 Acc: 0.9608

Epoch 10/24
----------
train Loss: 0.3331 Acc: 0.8607
val Loss: 0.1892 Acc: 0.9477

Epoch 11/24
----------
train Loss: 0.3621 Acc: 0.8238
val Loss: 0.2312 Acc: 0.9150

Epoch 12/24
----------
train Loss: 0.3467 Acc: 0.8484
val Loss: 0.1750 Acc: 0.9477

Epoch 13/24
----------
train Loss: 0.3983 Acc: 0.8484
val Loss: 0.1668 Acc: 0.9542

Epoch 14/24
----------
train Loss: 0.3813 Acc: 0.8402
val Loss: 0.1643 Acc: 0.9477

Epoch 15/24
----------
train Loss: 0.3300 Acc: 0.8566
val Loss: 0.1960 Acc: 0.9412

Epoch 16/24
----------
train Loss: 0.3250 Acc: 0.8730
val Loss: 0.1593 Acc: 0.9542

Epoch 17/24
----------
train Loss: 0.3146 Acc: 0.8730
val Loss: 0.1712 Acc: 0.9608

Epoch 18/24
----------
train Loss: 0.4021 Acc: 0.8197
val Loss: 0.1673 Acc: 0.9412

Epoch 19/24
----------
train Loss: 0.3269 Acc: 0.8443
val Loss: 0.1795 Acc: 0.9542

Epoch 20/24
----------
train Loss: 0.2452 Acc: 0.8893
val Loss: 0.1798 Acc: 0.9477

Epoch 21/24
----------
train Loss: 0.3460 Acc: 0.8402
val Loss: 0.1740 Acc: 0.9542

Epoch 22/24
----------
train Loss: 0.3517 Acc: 0.8484
val Loss: 0.1767 Acc: 0.9542

Epoch 23/24
----------
train Loss: 0.3237 Acc: 0.8648
val Loss: 0.1625 Acc: 0.9542

Epoch 24/24
----------
train Loss: 0.3714 Acc: 0.8484
val Loss: 0.1630 Acc: 0.9608

Training complete in 39m 58s
Best val Acc: 0.960784



## GeForce GT 520M - dell xps 14z

Abdelkrim tried to downgrade pytorch to cudatoolkit 9.0 but the minimum capability is 3.5

command : `conda install pytorch torchvision cudatoolkit=9.0 -c pytorch -c defaults -c numba/label/de`

C:\App\Miniconda2\envs\py37\lib\site-packages\torch\cuda\__init__.py:118: UserWarning:
    Found GPU0 GeForce GT 520M which is of cuda capability 2.1.
    PyTorch no longer supports this GPU because it is too old.
    The minimum cuda capability that we support is 3.5.

  warnings.warn(old_gpu_warn % (d, name, major, capability[1]))
  
GeForce GT 520M
Memory Usage:
Allocated: 0.0 GB
Cached:    0.0 GB


### run python transfer_learning_tutorial.py

python transfer_learning_tutorial.py

  C:\App\Miniconda2\envs\py37\lib\site-packages\torch\cuda\__init__.py:118: UserWarning: 
      Found GPU0 GeForce GT 520M which is of cuda capability 2.1.
      PyTorch no longer supports this GPU because it is too old.
      The minimum cuda capability that we support is 3.5.

    warnings.warn(old_gpu_warn % (d, name, major, capability[1]))

  Epoch 0/24
  ----------
  Traceback (most recent call last):
    File "transfer_learning_tutorial.py", line 199, in <module>
      num_epochs=25)
    File "transfer_learning_tutorial.py", line 113, in train_model
      outputs = model(inputs)
    File "C:\App\Miniconda2\envs\py37\lib\site-packages\torch\nn\modules\module.py", line 493, in __call__
      result = self.forward(*input, **kwargs)
    File "C:\App\Miniconda2\envs\py37\lib\site-packages\torchvision\models\resnet.py", line 192, in forward
      x = self.conv1(x)
    File "C:\App\Miniconda2\envs\py37\lib\site-packages\torch\nn\modules\module.py", line 493, in __call__
      result = self.forward(*input, **kwargs)
    File "C:\App\Miniconda2\envs\py37\lib\site-packages\torch\nn\modules\conv.py", line 338, in forward
      self.padding, self.dilation, self.groups)
  RuntimeError: cuDNN error: CUDNN_STATUS_ARCH_MISMATCH