#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import time
import logging


class Image(object):

    cnt = 0
    # source :
    # https://stackoverflow.com/questions/43462896/how-do-i-download-images-with-an-https-url-in-python-3/43463333

    def createFilePathExport(self, filePathExport):
        # source :
        # https://www.tutorialspoint.com/How-can-I-create-a-directory-if-it-does-not-exist-using-Python
        import os
        import errno
        try:
            os.makedirs(filePathExport)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

    def readImage(self, url):
        import urllib.request
        from socket import timeout
        try:
            with urllib.request.urlopen(url) as response:
                return response.read()
        except urllib.request.URLError as e:
            # http://www.enseignement.polytechnique.fr/informatique/INF478/docs/Python3/howto/urllib2.html
            # print(str(e.code) + ' ' + e.reason)
            if hasattr(e, 'reason'):
                print('We failed to reach a server.')
                print('Reason: ', e.reason)
            elif hasattr(e, 'code'):
                print('The server couldn\'t fulfill the request.')
                print('Error code: ', e.code)
            return None
        except timeout:
            logging.error('socket timed out - URL %s', url)
            return None

    def isImage(self, theImage):
        import imghdr
        imageType = imghdr.what('', h=theImage)
        print('image type : ' + str(imageType))
        # https://docs.python.org/3/library/imghdr.html#imghdr.what
        acceptedtypes = ('rgb',
                         'gif',
                         'pbm',
                         'pgm',
                         'ppm',
                         'tiff',
                         'rast',
                         'xbm',
                         'jpeg',
                         'bmp',
                         'png',
                         'webp',
                         'exr')
        if imageType in acceptedtypes:
            return True
        else:
            return False

    def downloadImages(self, filename, filePathUrls, filePathExport):
        import datetime
        from urllib.parse import urlparse
        self.createFilePathExport(filePathExport=filePathExport)
        fileContainingUrls = os.path.join(filePathUrls, filename)
        version = datetime.datetime.today().strftime('%Y.%m.%d')
        release = version + '-' + datetime.datetime.today().strftime('%H.%M')

        try:
            with open(fileContainingUrls) as fp:
                url = fp.readline()
                while url:
                    print("Line {}: {}".format(self.cnt, url.strip()))
                    outputFilename = release + '-' + \
                        "{:04d}".format(self.cnt) + '-' + \
                        (urlparse(url).path.rsplit('/', 1)[-1]).rstrip()
                    print("Output {}: {}".format(self.cnt, outputFilename))
                    with open(os.path.join(filePathExport, outputFilename),
                              'wb') as f:
                        try:
                            theImage = self.readImage(url)
                            if theImage is not None:
                                if self.isImage(theImage):
                                    f.write(theImage)
                        except:
                            print("An exception occurred")

                    url = fp.readline()
                    self.cnt += 1
                print("urls downloaded:" + self.cnt)

        finally:
            fp.close()


if __name__ == '__main__':
    since = time.time()
    img = Image()
    img.downloadImages(filename='bees.images.urls.txt',
                       filePathUrls=os.path.join(os.path.dirname(
                           os.path.abspath(__file__)), 'data', 'image-net.org',
                           'urls'),
                       filePathExport=os.path.join(os.path.dirname(
                           os.path.abspath(__file__)), 'data', 'image-net.org',
                           'export')
                       )
    # check whether a new image was get or not

    print('Download completed! : ' + str(img.cnt) + ' items')

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(
        time_elapsed // 60, time_elapsed % 60))
