import torch

torch.__version__

device = torch.cuda.current_device()
print(device)

torch.cuda.device(device)

print(torch.cuda.device_count())

print(torch.cuda.get_device_name(0))

print(torch.cuda.is_available())
