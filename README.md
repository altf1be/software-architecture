# README

Read the documentation on Read the docs : <https://alt-f1-software-architecture.readthedocs.io/en/latest/>

(c) alt-f1 sprl http://www.alt-f1.be 

## What is this repository for?

* Document the best practices for Software engineers

## release as a target type

    git checkout master; git pull origin master
    standard-version --first-release
    standard-version
    npm run release -- --release-as patch | minor | major 
    git push --follow-tags origin master

## Sphinx font

    See https://tex.stackexchange.com/questions/152721/problems-with-fonts/152749
    Choose a font on https://tug.org/FontCatalogue/
        `initexmf --admin --mkmaps (or  updmap --admin)`
        `initexmf --edit-config-file updmap`
            #ccicons
            Map ccicons.map
            Map <font name>.map


## License

This work is licensed under a Creative Commons Attribution 4.0 International License : <http://creativecommons.org/licenses/by/4.0>