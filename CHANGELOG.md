# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.1.56](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.55...v1.1.56) (2021-03-23)


### Bug Fixes

* add installation of git lfs ([0646edc](https://bitbucket.org/altf1be/software-architecture/commit/0646edcc68d8daa4373dc8b815ab66448898a2c7))

### [1.1.55](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.54...v1.1.55) (2021-03-23)


### Continous improvements (CI)

* add in requirements.txt docxbuilder graphviz nbsphinx pandoc plantweb recommonmark sphinxcontrib-bibtex Unidecode ([df8a2f7](https://bitbucket.org/altf1be/software-architecture/commit/df8a2f7eabf93544998cb6bc8828838076af2587))

### [1.1.54](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.53...v1.1.54) (2021-03-23)


### Continous improvements (CI)

* add dynaconf in requirements.txt ([5cbcbad](https://bitbucket.org/altf1be/software-architecture/commit/5cbcbadefd19d40b2b5be529c4a25d10ac68d03c))

### [1.1.53](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.52...v1.1.53) (2021-03-23)


### Continous improvements (CI)

* add ipykernel in requirements.txt ([f47c212](https://bitbucket.org/altf1be/software-architecture/commit/f47c212b0ac38d148a51f2bc8b3a1313e980d55e))


### Refactorings

* use the new version of the conf.py inclusing the usage of .toml file containing the metadata about the project ([29d1372](https://bitbucket.org/altf1be/software-architecture/commit/29d1372e48f7905ffe6988c2cb63e6c15e29dee0))


### Documentations

* add the section about Kafka ([dcbb5aa](https://bitbucket.org/altf1be/software-architecture/commit/dcbb5aab9052bce8e16487b11c95a017391c6726))
* add the section about the Authentic Source ([606cab5](https://bitbucket.org/altf1be/software-architecture/commit/606cab52c7656813d54a88854770c972f4436f02))
* add the section about the Authentic Source ([9c94b9b](https://bitbucket.org/altf1be/software-architecture/commit/9c94b9bdb6edb9405930a1445399e626ca1aef1f))
* add the section about the ETL ([10077ef](https://bitbucket.org/altf1be/software-architecture/commit/10077ef2daa4fcf8ebd5e884644964899df0a6e1))
* add the section about the hack to build a Netflix/OTT service ([2f280dd](https://bitbucket.org/altf1be/software-architecture/commit/2f280dd1e38eb9ff5642499e84478b83686b96d6))
* add the section about the Programme Management ([41dcc5a](https://bitbucket.org/altf1be/software-architecture/commit/41dcc5a57b32a760db7d12d5c2de49b5f753a116))
* add the section about the Programme Management ([da221ee](https://bitbucket.org/altf1be/software-architecture/commit/da221eebcbf5ee6a6d549bb2bd4637140cdf2aff))
* add the section about the web scraping ([25e1118](https://bitbucket.org/altf1be/software-architecture/commit/25e11182c86325c4b721cb03e76697bc4f0c6489))
* add the secure and private AI chapters in the index including the jupyter notebooks ([8a079bc](https://bitbucket.org/altf1be/software-architecture/commit/8a079bc2f93811249d00d99db96b247917a411fe))
* add the title ([13893f6](https://bitbucket.org/altf1be/software-architecture/commit/13893f687580604b08718bdf48e66c5b76988c0a))
* add the value chain for software product delivery ([28922e8](https://bitbucket.org/altf1be/software-architecture/commit/28922e8952f20ce7c67b6cf6da6ebe5f478a61da))


### Chores

* add _templates including new latex and docx ([59f941e](https://bitbucket.org/altf1be/software-architecture/commit/59f941e3cdd92b4d26094c8a6753c75ed9330a2a))
* add plantuml 1.2021.1 ([a6620fa](https://bitbucket.org/altf1be/software-architecture/commit/a6620fa77cec71953ae52e625ce2fcd9067f7b37))
* move/add shell scripts under sh/ ([da9e47b](https://bitbucket.org/altf1be/software-architecture/commit/da9e47bb04962d7573146ab97f3f8761039596b3))
* move/add shell scripts under sh/ ([2efe8bb](https://bitbucket.org/altf1be/software-architecture/commit/2efe8bbc441941416a92f2fa66aebf436589fcaf))

### [1.1.52](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.51...v1.1.52) (2020-12-28)


### Documentations

* update/add: Digitization of Insurances, Web Scraping, HR Rules, Glossary ([7aad276](https://bitbucket.org/altf1be/software-architecture/commit/7aad276229d527891ea533840590526055efbd58))


### Chores

* remove git-lfs executable files used by readthedocs.org but useless for local repository ([1266348](https://bitbucket.org/altf1be/software-architecture/commit/12663487a7c077734aeda2a062c301148bc5e21c))

### [1.1.51](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.50...v1.1.51) (2020-12-17)


### Bug Fixes

* add_stylesheet -> add_css_file ([fc9b809](https://bitbucket.org/altf1be/software-architecture/commit/fc9b8095ff535298faeff9d21238ab744ac0c4d3))


### Chores

* set the conda environment to 'sphinx' ([5b4c94b](https://bitbucket.org/altf1be/software-architecture/commit/5b4c94b9ff1a26a2f4d074f7ffaee5a3657e1948))
* update standard-version to v9.0.0 ([4e4c560](https://bitbucket.org/altf1be/software-architecture/commit/4e4c5609fe231b25ca15c2525b4651e39a5d1a7d))


### Documentations

* add the screenshot of the dunning service, Swagger home page ([ddaa24e](https://bitbucket.org/altf1be/software-architecture/commit/ddaa24e5c1be65a51c413d486b1eca0e19a723c7))
* **fix:** the terms a capitalized ([cbb7084](https://bitbucket.org/altf1be/software-architecture/commit/cbb7084051e9aa800b4e641cf4eca9c71e55ddee))
* **fix:** the terms a capitalized ([cd0ec35](https://bitbucket.org/altf1be/software-architecture/commit/cd0ec35b3e486e394033d79c581de1a8389ec49e))

### [1.1.50](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.49...v1.1.50) (2020-04-17)


### Documentations

* **fix:** the sentence was in bold and cut in the PDF version. now the complete bullet point will be displayed in the PDF ([109df52](https://bitbucket.org/altf1be/software-architecture/commit/109df52831da98f2a1acdc7dd5f49bc9d630f4b8))

### [1.1.49](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.48...v1.1.49) (2020-04-17)


### Features

* **fix:** download files in git-lfs. here is a solution : https://test-builds.readthedocs.io/en/git-lfs/ ([c2f2d9b](https://bitbucket.org/altf1be/software-architecture/commit/c2f2d9b86f3db09e959410107f1118846ad766cf))

### [1.1.48](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.47...v1.1.48) (2020-04-17)


### Features

* Add Git LFS support to correctly manage the images. See https://github.com/readthedocs/readthedocs.org/issues/1846 ([fbe07c2](https://bitbucket.org/altf1be/software-architecture/commit/fbe07c28c012e448af88c2455885b5f48c3f9a4e))


### Continous improvements (CI)

* add git-lfs and sphinxcontrib-httpdomain to manage git-lfs. See https://github.com/readthedocs/readthedocs.org/issues/1846 and https://github.com/InfinniPlatform/InfinniPlatform.readthedocs.org.en/blob/master/requirements/pip.txt ([a227fa1](https://bitbucket.org/altf1be/software-architecture/commit/a227fa124dc650e39ce0803bef4deb5620ef76b4))
* try a new solution to manage git-lfs. see https://github.com/swistakm/pyimgui/blob/master/doc/source/conf.py#L36-L37 ([6e34209](https://bitbucket.org/altf1be/software-architecture/commit/6e34209b22643ad09796a3d6b373d7b25e4281c7)), closes [/github.com/swistakm/pyimgui/blob/master/doc/source/conf.py#L36-L37](https://bitbucket.org/altf1be//github.com/swistakm/pyimgui/blob/master/doc/source/conf.py/issues/L36-L37)

### [1.1.47](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.46...v1.1.47) (2020-03-15)


### Bug Fixes

* remobe unnecessary imports ([736e690](https://bitbucket.org/altf1be/software-architecture/commit/736e690e7d6040758d865e73f8daca049c6ea6d9))


### Chores

* add a formatter for the python code: black ([d882bad](https://bitbucket.org/altf1be/software-architecture/commit/d882badaec0214fa36059fbfb876bb846dbc1095))

### [1.1.46](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.45...v1.1.46) (2020-03-11)


### Bug Fixes

* add a new image of the value porter chain due to errors dureing the compilation on rtfd.org ([03f559e](https://bitbucket.org/altf1be/software-architecture/commit/03f559ebe37d24dc66ef99885fb06368a330fe0b))

### [1.1.45](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.44...v1.1.45) (2020-03-11)


### Bug Fixes

* add a new image of the value porter chain due to errors dureing the compilation on rtfd.org ([0e38ef2](https://bitbucket.org/altf1be/software-architecture/commit/0e38ef2a4c4a837a10ac801b3fbd005d881500e4))

### [1.1.44](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.43...v1.1.44) (2020-03-10)


### Bug Fixes

* chnage the image format due to an error during the build on rtfd.org ([bb5c1a1](https://bitbucket.org/altf1be/software-architecture/commit/bb5c1a198c4f30934a8f7712a5e8faaa03406386))

### [1.1.43](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.42...v1.1.43) (2020-03-10)


### Bug Fixes

* change the format of the image due to a crash during the build of the documentation on rtfd.org ([b871c28](https://bitbucket.org/altf1be/software-architecture/commit/b871c2845f33ae2ea044403e40f7c0643f925a57))

### [1.1.42](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.41...v1.1.42) (2020-03-10)


### Bug Fixes

* add missing json node : scripts ([35c52ec](https://bitbucket.org/altf1be/software-architecture/commit/35c52ec699a4a187347fac9e50cff2878a960a7b))
* chnage the image format due to an error during the build on rtfd.org ([360c3d9](https://bitbucket.org/altf1be/software-architecture/commit/360c3d966c68f97ab246fcb2af9628382012d295))


### Documentations

* add artificial intelligence and machine learning process ([2e89797](https://bitbucket.org/altf1be/software-architecture/commit/2e89797233aba73728c6b428b7e86856e3eadf75))
* add gartner learning workflow phases ([4472c11](https://bitbucket.org/altf1be/software-architecture/commit/4472c112dda2a9c9a655bce68058a3263aa37802))
* add gartner report: Accelerate Your Machine Learning and Artificial Intelligence Journey Using These DevOps Best Practices ([a43373d](https://bitbucket.org/altf1be/software-architecture/commit/a43373d66fca563052adc38f7a8b11fc9e28ab15))
* add porter value chain figure ([be1781f](https://bitbucket.org/altf1be/software-architecture/commit/be1781f4c710b234b69ff924358cba476f425d9b))
* add python, notes for professionals documenatation ([b401d64](https://bitbucket.org/altf1be/software-architecture/commit/b401d643aa97896e1793a6ff618574824d714bb1))
* add references to 'la librairie de l éco' ([3d71355](https://bitbucket.org/altf1be/software-architecture/commit/3d713556c45c357384cb141f9e50a3a423bf8351))


### Continous improvements (CI)

* add new scripts to build the changelog and manage the semver ([956e116](https://bitbucket.org/altf1be/software-architecture/commit/956e11630a31c67bab035b2c2e09afae070600a8))
* add the package-lock.json since the new release ([e2ba326](https://bitbucket.org/altf1be/software-architecture/commit/e2ba326228a330baf782610f9be186c5ef9eea60))
* ignore python files, jupyter notebook files and nodejs files ([09f345e](https://bitbucket.org/altf1be/software-architecture/commit/09f345e50b2611ccc499e7f999119e160934165a))
* set the name of headings within the changelog ([48d660c](https://bitbucket.org/altf1be/software-architecture/commit/48d660cb0b64f948b1d17a5ce29db7e7b385f270))


### Chores

* change tehe authors' name ([910780f](https://bitbucket.org/altf1be/software-architecture/commit/910780fe13abc28f3063fa4bd945d04282de7b55))
* remove plantuml from the libraries ([6412c1c](https://bitbucket.org/altf1be/software-architecture/commit/6412c1c36a73af202f971ff8fac56f36487fbc28))
* set pycode settings ([2a63064](https://bitbucket.org/altf1be/software-architecture/commit/2a630649ee9e2064a5f8ce877add8f1ccf378e00))
* set the parameters of vscode : black formatter and other s ([6408a27](https://bitbucket.org/altf1be/software-architecture/commit/6408a27301e18d7ca194dfc7bf5deebcef2f3604))

### [1.1.41](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.40...v1.1.41) (2019-10-03)



### [1.1.40](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.39...v1.1.40) (2019-09-30)



### [1.1.39](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.38...v1.1.39) (2019-09-30)



### [1.1.38](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.37...v1.1.38) (2019-09-30)



### [1.1.37](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.36...v1.1.37) (2019-09-30)



### [1.1.36](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.35...v1.1.36) (2019-09-30)



### [1.1.35](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.34...v1.1.35) (2019-09-30)



### [1.1.34](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.33...v1.1.34) (2019-09-30)



### [1.1.33](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.32...v1.1.33) (2019-09-30)



### [1.1.32](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.31...v1.1.32) (2019-09-30)



### [1.1.31](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.30...v1.1.31) (2019-09-23)



### [1.1.30](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.29...v1.1.30) (2019-09-23)



### [1.1.29](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.28...v1.1.29) (2019-09-23)



### [1.1.28](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.27...v1.1.28) (2019-09-23)


### Bug Fixes

* remove plantuml because it crashes on rtfd.org ([0523ab2](https://bitbucket.org/altf1be/software-architecture/commit/0523ab2))



### [1.1.27](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.26...v1.1.27) (2019-09-23)



### [1.1.26](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.25...v1.1.26) (2019-09-20)



### [1.1.25](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.24...v1.1.25) (2019-09-19)



### [1.1.24](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.23...v1.1.24) (2019-09-17)



### [1.1.23](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.22...v1.1.23) (2019-09-17)



## [1.1.22](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.21...v1.1.22) (2019-09-17)



## [1.1.21](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.20...v1.1.21) (2019-09-17)



## [1.1.20](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.19...v1.1.20) (2019-09-13)



### [1.1.19](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.18...v1.1.19) (2019-09-10)



### [1.1.18](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.17...v1.1.18) (2019-09-10)



### [1.1.17](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.16...v1.1.17) (2019-09-10)



### [1.1.16](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.15...v1.1.16) (2019-09-10)



### [1.1.15](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.14...v1.1.15) (2019-09-10)



### [1.1.14](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.13...v1.1.14) (2019-09-10)



### [1.1.13](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.12...v1.1.13) (2019-09-09)



### [1.1.12](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.11...v1.1.12) (2019-09-06)



### [1.1.11](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.10...v1.1.11) (2019-09-06)



### [1.1.10](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.9...v1.1.10) (2019-09-06)



### [1.1.9](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.8...v1.1.9) (2019-09-02)



## [1.1.8](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.7...v1.1.8) (2019-05-21)



## [1.1.7](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.6...v1.1.7) (2019-05-21)



## [1.1.6](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.5...v1.1.6) (2019-05-03)



## [1.1.5](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.4...v1.1.5) (2019-05-03)



## [1.1.4](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.3...v1.1.4) (2019-05-03)



## [1.1.3](https://bitbucket.org/altf1be/software-architecture/compare/v0.0.0...v1.1.3) (2019-05-03)



## [1.1.2](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.1...v1.1.2) (2019-04-22)



## [1.1.1](https://bitbucket.org/altf1be/software-architecture/compare/v1.1.0...v1.1.1) (2019-04-22)



# [1.1.0](https://bitbucket.org/altf1be/software-architecture/compare/v0.0.1...v1.1.0) (2019-04-22)


### Bug Fixes

* **chore:** update the version number to match highest value ([444e930](https://bitbucket.org/altf1be/software-architecture/commits/444e930))



## [0.0.1](https://bitbucket.org/altf1be/software-architecture/compare/v1.0.1...v0.0.1) (2019-04-22)


### Bug Fixes

* **chore:** rename buildall.bat to keep a kind of naming convention ([7522ef6](https://bitbucket.org/altf1be/software-architecture/commits/7522ef6))



# [0.0.0](https://bitbucket.org/altf1be/software-architecture/compare/v1.0.1...v0.0.0) (2019-05-02)

# [](https://bitbucket.org/altf1be/software-architecture/compare/v1.0.1...v) (2019-04-21)

# [](https://bitbucket.org/altf1be/software-architecture/compare/v1.0.0...v) (2019-04-18)

#  (2019-04-18)

### Bug Fixes

* **docs:** corrent the format of the NGROK documentation ([aab8497](https://bitbucket.org/altf1be/software-architecture/commits/aab8497))
* Could not import extension sphinxcontrib.gist on rtfd.org ([4e61a4a](https://bitbucket.org/altf1be/software-architecture/commits/4e61a4a))
* **chore:** adapt the variables to display the name of the project including spaces in the PDF ([35731a0](https://bitbucket.org/altf1be/software-architecture/commits/35731a0))
* **chore:** delete build\latexpdf when build.pdf.bat is launched ([ca994a5](https://bitbucket.org/altf1be/software-architecture/commits/ca994a5))
* **chore:** rename image to match the required case sensitiveness ([6eacccd](https://bitbucket.org/altf1be/software-architecture/commits/6eacccd))
* **chore:** rename image to match the required case sensitiveness ([66d8f99](https://bitbucket.org/altf1be/software-architecture/commits/66d8f99))
* **chore:** rename images, indicate the size of the images ([7ad34da](https://bitbucket.org/altf1be/software-architecture/commits/7ad34da))
* **docs:** add a backup strategy ([6881377](https://bitbucket.org/altf1be/software-architecture/commits/6881377))
* **docs:** add missing spaces for bullet points ([f97bab5](https://bitbucket.org/altf1be/software-architecture/commits/f97bab5))
* **docs:** correct the heading 1 ([0123490](https://bitbucket.org/altf1be/software-architecture/commits/0123490))
* **docs:** improve the quality of the titles + add picture of ci-cd by microsoft ([7e2ec66](https://bitbucket.org/altf1be/software-architecture/commits/7e2ec66))
* **docs:** reduce the scale of the images to be displayed correctly on a PDF ([333edce](https://bitbucket.org/altf1be/software-architecture/commits/333edce))
* **gist:** rtfd.org crashes when sphinxcontrib.gist is enabled, replaced the ..gist by ..raw :: html ([64c780c](https://bitbucket.org/altf1be/software-architecture/commits/64c780c))
* remove spaces of the project name because it crashes the generation of the pdf ([a10766d](https://bitbucket.org/altf1be/software-architecture/commits/a10766d))
* removed the version of the requirement ([ecc73fb](https://bitbucket.org/altf1be/software-architecture/commits/ecc73fb))

### Features

* add a 'badge' indicating if the build of the documentation succeeded on https://www.rtfd.org ([09bbda4](https://bitbucket.org/altf1be/software-architecture/commits/09bbda4))
* add ALT-F1-Application_deployment.rst ALT-F1-Bug_template.rst ALT-F1-Curriculum_DotNet.rst ALT-F1-GDPR.rst ALT-F1-Research_and_development.rst ALT-F1-Sales_and_Marketing.rst ALT-F1-Technologies.rst ([bfaf451](https://bitbucket.org/altf1be/software-architecture/commits/bfaf451))
